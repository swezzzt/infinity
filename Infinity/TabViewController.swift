//
//  TabViewController.swift
//  Infinity
//
//  Created by FLK on 15/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class TabViewController: UIViewController {

  var _viewControllers = [UIViewController]()
  let verticalSpace:CGFloat = 100

  let scrollView: UIScrollView = {
    let v = UIScrollView()
    v.backgroundColor = .clear
    return v
  }()

  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    self.view.backgroundColor = .brown
    self.view.addSubview(self.scrollView)
    self.scrollView.delegate = self
    self.scrollView.alwaysBounceHorizontal = false
    self.scrollView.alwaysBounceVertical = true
    self.scrollView.clipsToBounds = false
    self.scrollView.showsVerticalScrollIndicator = false
    self.scrollView.snp.makeConstraints { make in
      make.edges.equalToSuperview().inset(UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30))
    }

   // self.fakescrollView.snp.makeConstraints { make in
     // make.edges.equalToSuperview().inset(UIEdgeInsets(top: 30, left: 30, bottom: 30, right: 30))
    //}

    let num:CGFloat = 1
    self.scrollView.contentSize = CGSize(width: self.view.frame.size.width - 60,
                                         height: (self.view.frame.size.height - 60) + (num-1)*verticalSpace)

    for i in stride(from: 0, to: Int(num), by: 1) {
      let nvc = SimpleViewController(index: i)
      self.addChild(nvc)
      _viewControllers.append(nvc)
      nvc.willMove(toParent: self)
      self.scrollView.addSubview(nvc.view)
      nvc.view.snp.makeConstraints { make in
        make.width.equalToSuperview()
        make.height.equalToSuperview()
        make.top.equalTo( CGFloat(i) * self.verticalSpace )
        make.centerX.equalToSuperview()
      }
      nvc.didMove(toParent: self)
    }


  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.
    //let panGestureRecognizer = UIPanGestureRecognizer(target: self,
    //                                                  action: #selector(MasterViewController.handlePanGesture(recognizer:)))
    //panGestureRecognizer.delegate = self
    //self.view.addGestureRecognizer(panGestureRecognizer)
  }


  func verticalSpaceForIndex(index: Int) {

  }

  func layoutViewsForPercentageOffset(point: CGPoint) {
    //print("layoutViewsForPercentageOffsetlayoutViewsForPercentageOffsetlayoutViewsForPercentageOffsetlayoutViewsForPercentageOffset")
    //var i = 0
    let progress = point.y
    let numberOfStep = CGFloat(_viewControllers.count - 1)
    let stepProgress = 1 / numberOfStep

    //var realProgress = 1 - progress
    //var newTopOffset:CGFloat = 0// + scrollView.contentOffset.y

    var lastTop:CGFloat = 0
    //var lastDiff:CGFloat = 0

    //newTopOffset = scrollView.contentOffset.y
    //print("NEW LOOP ======= \(progress)")
    for i in stride(from: 0, to: _viewControllers.count, by: 1) {

      //if realProgress > 1 || realProgress < 0 {
      //  //print("Bouncing > Exit")
      //  continue
      //}

      let iF = CGFloat(i)
      let previousExists = _viewControllers.indices.contains(i-1)
      //let nextExists = _viewControllers.indices.contains(i+1)

      let vc = _viewControllers[i]
      let lastStep:CGFloat = (iF-1) * stepProgress
      let currentStep:CGFloat = (iF) * stepProgress

      if previousExists {

        if progress >= lastStep && progress < currentStep {
          let progressInStep = (progress - lastStep) * ( 1 / ( currentStep - lastStep ))
          //print("\(i) should move \(lastStep) \(currentStep) %\(progressInStep)")
          //print("\(i) newTop = \(self.verticalSpace * progressInStep) ")
          lastTop = self.verticalSpace * (floor(progress / currentStep) + 1) - self.verticalSpace * progressInStep
          vc.view.snp.updateConstraints{ make in
            make.top.equalTo( lastTop + abs(scrollView.contentOffset.y) )
          }
        } else if progress >= currentStep {
          //print("\(i) should NOT move")
          vc.view.snp.updateConstraints{ make in
            make.top.equalTo( abs(scrollView.contentOffset.y) )
          }
        } else {
          vc.view.snp.updateConstraints{ make in
            lastTop += self.verticalSpace
            make.top.equalTo( lastTop + abs(scrollView.contentOffset.y) )
          }
        }
      } else if !previousExists {
        //print("Animating First \(scrollView.contentOffset.y)")
        vc.view.snp.updateConstraints{ make in
          make.top.equalTo( abs(scrollView.contentOffset.y) )
        }
      }

      if progress > 1 {
        //print("Bouncing TOP \(scrollView.contentOffset.y) \(scrollView.contentSize.height)")
        vc.view.snp.updateConstraints{ make in
          make.top.equalTo( self.verticalSpace * numberOfStep )
        }
      }
    }
    //print("First VC TOP = \(_viewControllers[i].view.frame.origin.y)")

/*
    for vc in _viewControllers {
      continue
      //var cFrame = vc.view.frame
      let cIndex = self.indexOfViewController(viewController: vc)

      //if i == 0 {
      //  newTopOffset = scrollView.contentOffset.y
    //  } else {



      continue
      var newAddedOffset:CGFloat = 0//(self.verticalSpace * realProgress) * cIndex

      if newAddedOffset > self.verticalSpace {
        //newAddedOffset = self.verticalSpace
      }

      if cIndex != 0 && lastDiff != self.verticalSpace {
       // newAddedOffset = self.verticalSpace
      }

      //if realProgress > 1 {
        newAddedOffset = -scrollView.contentOffset.y + self.verticalSpace * cIndex
      //}

      newTopOffset = newAddedOffset
      print("\(scrollView.contentOffset.y) \(realProgress) from \(cIndex) to \(cIndex-1) = \(newTopOffset - lastTop) | lastDiff=\(lastDiff) | newAddedOffset = \(newAddedOffset) newTopOffset = \(newTopOffset)")

      lastDiff = newTopOffset - lastTop
      lastTop = newTopOffset



      continue
      newTopOffset = (self.verticalSpace * realProgress) * cIndex

      //}
      if newTopOffset == 0 && realProgress < 1 {
        //newTopOffset = scrollView.contentOffset.y
      }

      if newTopOffset < lastTop {
        newTopOffset = lastTop
      } else if newTopOffset - lastTop > self.verticalSpace {
        newTopOffset = lastTop + self.verticalSpace
      }



      if lastDiff > 0 && newTopOffset - lastTop < self.verticalSpace {
        newTopOffset = lastTop + self.verticalSpace
      }

      print("to-co \(newTopOffset - scrollView.contentOffset.y)")

      print("cIndex \(cIndex) | lastDiff = \(newTopOffset - lastTop)")
/*
      if realProgress < 0 {
        newTopOffset -= 50
      }
*/
      if i == 0 {
        //print("Progress = \(progress) || realProgress \(realProgress) || offset \(scrollView.contentOffset.y)")
        //print("newTopOffset = \(newTopOffset)")
      }

      vc.view.snp.updateConstraints{ make in
        make.top.equalTo( newTopOffset )
      }
      lastTop = newTopOffset
      lastDiff = newTopOffset - lastTop
      i += 1
      continue



      var realOffset = scrollView.contentOffset.y
      if realProgress > 1 {
        realProgress = 1
        //realOffset = cIndex * 5
      } else if realProgress < 0 {
        realProgress = 0
        //realOffset = cIndex*5
      }

      if progress > 1 {

        realOffset = scrollView.contentOffset.y - (progress - 1) * 500//cIndex * 5 * progress// + scrollView.contentOffset.y
      } else if progress <= 0 {
        realOffset = 0
      }


      var contentOffset:CGFloat = realOffset
      var cOffset:CGFloat = (95  * ( realProgress ) + 5) * cIndex
      var newTopOffset = contentOffset + cOffset
      //max( cIndex * 5, cIndex * 100.0 * (1 - progress))
      //print("cIndex = \(cIndex) || TopOffset = \(newTopOffset) === min\(-cIndex * 100.0) max\(cIndex * 100.0)")

      //newTopOffset = min(newTopOffset, -cIndex * 100.0)
      //newTopOffset = max(newTopOffset, cIndex * 100.0)
      /*
      if newTopOffset >= (contentOffset + cIndex * 100.0) {
        newTopOffset = cIndex * 100.0
      } else if newTopOffset <= (contentOffset + -cIndex * 100.0) {
        newTopOffset = -cIndex * 100.0

      }
      */
/*
      if progress > 1 {
        newTopOffset += (cIndex + 1) * -5
      }
*/
        //print("\(scrollView.contentOffset.y) + 100 + max(\(cIndex * 5)|\(cIndex * 100.0 * (1 - point.y))) == \(newTopOffset)")
        //print("minus = \(((point.y - 1) * 500))")



    }
 */
  }
/*
  func indexOfViewController(viewController: UIViewController) -> CGFloat {
    return CGFloat(self._viewControllers.index(of: viewController)!)
  }
*/
}


extension TabViewController: UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {

    // horizontal
    //let maximumHorizontalOffset = scrollView.contentSize.width - scrollView.frame.height
    //let currentHorizontalOffset = scrollView.contentOffset.x

    // vertical
    let maximumVerticalOffset = scrollView.contentSize.height - scrollView.frame.height
    let currentVerticalOffset = scrollView.contentOffset.y

    // percentages
    //let percentageHorizontalOffset = currentHorizontalOffset / maximumHorizontalOffset
    let percentageVerticalOffset = currentVerticalOffset / maximumVerticalOffset

    self.layoutViewsForPercentageOffset(point: CGPoint(x: 0, y: percentageVerticalOffset))
    //[self scrollView:scrollView didScrollToPercentageOffset:CGPointMake(percentageHorizontalOffset, percentageVerticalOffset)];

  }
}
