//
//  SmileysViewController.swift
//  Infinity
//
//  Created by FLK on 10/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

protocol SmileyViewDelegate: NSObjectProtocol {
  func didSelectSmiley(smiley: Smiley)
}

enum SmileySectionType {
  case builtin
  case starred
  case search
}

struct SmileySection: Hashable {
  var hashValue: Int

  static func == (lhs: SmileySection, rhs: SmileySection) -> Bool {
    return (lhs.hashValue == rhs.hashValue)
  }

  var name: String
  var smileys: [Smiley]
  var type: SmileySectionType

  init(name: String, smileys: [Smiley]?, type: SmileySectionType = .builtin) {
    self.name = name
    self.hashValue = name.hashValue
    if let smile = smileys {
      self.smileys = smile
    } else {
      self.smileys = [Smiley]()
    }

    self.type = type
  }

}

class SmileysViewController: UIViewController {

  var sections = [SmileySection]()

  var collectionView: UICollectionView!
  weak var delegate: SmileyViewDelegate?

  init(sections: [SmileySection]) {
    self.sections = sections
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    let layout = SmileySectionViewFlowLayout()
    layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    layout.sectionHeadersPinToVisibleBounds = true


    collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.register(SmileyCollectionViewCell.self,
                            forCellWithReuseIdentifier: "smileyCell")

    collectionView.register(SmileyCollectionReusableHeaderView.self,
                            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                            withReuseIdentifier: "headerView")
    self.view.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1)
    collectionView.backgroundColor = .clear//UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1)
    collectionView.alwaysBounceVertical = true
    self.view.addSubview(self.collectionView!)
    self.collectionView?.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }

}

class SmileySectionViewFlowLayout : UICollectionViewFlowLayout {
  override func prepare() {
    super.prepare()

    register(SCSBCollectionReusableView.self, forDecorationViewOfKind: "sectionBackground")
  }

  override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
    let attributes = super.layoutAttributesForElements(in: rect)
    var allAttributes = [UICollectionViewLayoutAttributes]()

    if let attributes = attributes {

      for attr in attributes {
        // Look for the first item in a row
        // You can also calculate it by item (remove the second check in the if below and change the tmpWidth and frame origin
        if (attr.representedElementCategory == UICollectionView.ElementCategory.cell && attr.frame.origin.x == self.sectionInset.left) {

          // Create decoration attributes
          let decorationAttributes = SCSBCollectionViewLayoutAttributes(forDecorationViewOfKind: "sectionBackground", with: attr.indexPath)
            //SCSBCollectionViewLayoutAttributes(forDecorationViewOfKind: "sectionBackground", withIndexPath: attr.indexPath)
          // Set the color(s)
          decorationAttributes.color = .white
/*
          if (attr.indexPath.section % 2 == 0) {
            decorationAttributes.color = UIColor.green.withAlphaComponent(0.5)
          } else {
            decorationAttributes.color = UIColor.blue.withAlphaComponent(0.5)
          }
*/
          // Make the decoration view span the entire row
          let tmpWidth = self.collectionView!.contentSize.width
          let tmpHeight = attr.frame.size.height + self.sectionInset.top + self.sectionInset.bottom
          //self.itemSize.height + self.minimumLineSpacing + self.sectionInset.top / 2 + self.sectionInset.bottom / 2  // or attributes.frame.size.height instead of itemSize.height if dynamic or recalculated
          decorationAttributes.frame = CGRect(x: 0, y: attr.frame.origin.y - self.sectionInset.top,
                                              width: tmpWidth, height: tmpHeight)

          // Set the zIndex to be behind the item
          decorationAttributes.zIndex = attr.zIndex - 1

          // Add the attribute to the list
          allAttributes.append(decorationAttributes)
        }
      }
      // Combine the items and decorations arrays
      allAttributes.append(contentsOf: attributes)
    }

    return allAttributes
  }
}

class SCSBCollectionViewLayoutAttributes : UICollectionViewLayoutAttributes {
  var color: UIColor = .white

  override func copy(with zone: NSZone? = nil) -> Any {
    let newAttributes: SCSBCollectionViewLayoutAttributes = super.copy(with: zone) as! SCSBCollectionViewLayoutAttributes
    newAttributes.color = self.color.copy(with: zone) as! UIColor
    return newAttributes
  }
}

class SCSBCollectionReusableView : UICollectionReusableView {

  override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
    super.apply(layoutAttributes)

    let scLayoutAttributes = layoutAttributes as! SCSBCollectionViewLayoutAttributes
    self.backgroundColor = scLayoutAttributes.color
  }
}

extension SmileysViewController: UICollectionViewDelegateFlowLayout {

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    if sections[indexPath.section].type == .builtin {
      return CGSize(width: 40, height: 40)
    } else {
      return CGSize(width: 60, height: 50)
    }
  }

  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      referenceSizeForHeaderInSection section: Int) -> CGSize {

    let fakeHeader = SmileyCollectionReusableHeaderView(frame: .zero)
    fakeHeader.configure(with: "Boop")
    let dynamicSize = fakeHeader.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)

    return CGSize(width: 0, height: dynamicSize.height)
  }

  func collectionView(_ collectionView: UICollectionView,
                      viewForSupplementaryElementOfKind kind: String,
                      at indexPath: IndexPath) -> UICollectionReusableView {

    let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                               withReuseIdentifier: "headerView",
                                                               for: indexPath) as! SmileyCollectionReusableHeaderView

    header.configure(with: self.sections[indexPath.section].name)
    return header
  }
}
extension SmileysViewController: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    self.delegate?.didSelectSmiley(smiley: sections[indexPath.section].smileys[indexPath.row])
  }

  func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath)
    UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
      cell?.backgroundColor = .gray
    })
  }

  func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath)
    UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
      cell?.backgroundColor = .white
    })
  }

}

extension SmileysViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    //print(self.sections[section].smileys.count)
    return self.sections[section].smileys.count
  }



  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return self.sections.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = self.collectionView?.dequeueReusableCell(withReuseIdentifier: "smileyCell", for: indexPath) as! SmileyCollectionViewCell

    let smiley = sections[indexPath.section].smileys[indexPath.row]
    cell.configure(with: smiley)

    return cell
  }

/*
  func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
    print("willDisplaySupplementaryView")
    view.backgroundColor = .white
  }
  */
}
