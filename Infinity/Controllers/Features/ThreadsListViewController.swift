//
//  ThreadsListViewController.swift
//  Infinity
//
//  Created by FLK on 07/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
import PromiseKit
import FontAwesome_swift
import SVPullToRefresh

// MARK: - Protocols
protocol AuthenticationDelegate: NSObjectProtocol {
  func logged()
}
protocol FilterListViewDelegate: NSObjectProtocol {
  func selectFilter(_ filter: SectionFilter)
}
protocol ThreadsListDelegate: NSObjectProtocol {
  func selectThread(_ thread: Thread, initialPageType: ThreadPageType)
  func selectSection(_ section: Section, filter: SectionFilter)
}

extension ThreadsListDelegate {
  func selectThread(_ thread: Thread, initialPageType: ThreadPageType) {

  }

  func selectSection(_ section: Section, filter: SectionFilter) {
    
  }
}

// MARK: - Structs
struct ThreadSection: Hashable {
  var hashValue: Int

  static func == (lhs: ThreadSection, rhs: ThreadSection) -> Bool {
    return (lhs.hashValue == rhs.hashValue)
  }
  var section: Section? // Either nil (when it's a page, we just show the number) or a Section (when we want the section header to be tappable)
  var name: String
  var threadIDs: [ThreadID]

  init(name: String, threadIDs: [ThreadID]) {
    self.name = name
    self.hashValue = name.hashValue
    self.threadIDs = threadIDs
  }

  init(section: Section, threadIDs: [ThreadID]) {
    self.init(name: section.name, threadIDs: threadIDs)
    self.section = section
  }
}

// MARK: - ThreadsListViewController
class ThreadsListViewController: UIViewController {

  weak var delegate: ThreadsListDelegate?

  var sections = [ThreadSection]()
  var threads = [ThreadID: Thread]()

  var listType: ThreadType
  var formInputs: FormInputs?

  var selectedSection: Section?
  var currentFilter: SectionFilter

  var tableView = UITableView()
  var statusLabel = UILabel(frame: .zero)

  var lastLoadedPage: String?

  // FIXME: its crap
  lazy var longPress: UILongPressGestureRecognizer = {
    let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(showContextMenu(gesture:)))
    return longPressGesture
  }()

  // MARK: - Methods
  init(forType type: ThreadType) {
    self.listType = type
    self.currentFilter = self.listType.defaultFilter()
    super.init(nibName: nil, bundle: nil)
  }

  init(with section: Section, filter: SectionFilter) {
    self.listType = .section
    self.selectedSection = section
    self.currentFilter = filter
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    print("TLVC > viewDidLoad")
    super.viewDidLoad()

    self.view.backgroundColor = .clear
    let dropDownTitle = DropDownTitleView(title: "Sujets", subtitle: "Toutes")
    //dropDownTitle?.delegate = self
    self.navigationItem.titleView = dropDownTitle
    //self.navigationItem.title = ""

    self.tableView = UITableView(frame: .zero, style: .plain)

    self.tableView.delegate = self
    self.tableView.dataSource = self
    self.tableView.backgroundColor = UIColor(red: 239/255.0, green: 239/255.0, blue: 244/255.0, alpha: 1)
    self.tableView.register(ThreadTableViewCell.self, forCellReuseIdentifier: "threadCell")
    self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 1, height: 1))
    //self.tableView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: -60, right: 0)
    //self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: -60, right: 0)
    //self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
    //self.tableView.estimatedSectionHeaderHeight = 40

    self.view.addSubview(self.tableView)
    self.tableView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }

    self.configure(forNewListType: self.listType)

    self.configureNavigationBar()

    self.view.addGestureRecognizer(longPress)

    self.tableView.addPull(toRefresh: SVArrowPullToRefreshView.self, withActionHandler: { [unowned self] in
      self.refreshData()
    }, position: .top)

    self.tableView.addInfiniteScrolling { [unowned self] in
      self.loadNextPage()
    }

  }

  @objc func showContextMenu(gesture: UILongPressGestureRecognizer) {
    if gesture.state == UIGestureRecognizer.State.began {
      let touchPoint = gesture.location(in: self.tableView)
      print(touchPoint)
      if let indexPath = self.tableView.indexPathForRow(at: touchPoint) {

        let thread = self.threads[sections[indexPath.section].threadIDs[indexPath.row]]!
        //print("contextMenu for \(thread.name)")

        let alertController = UIAlertController(title: "Aller à...", message: nil, preferredStyle: .alert)
        let firstPageAction = UIAlertAction(title: "la première page", style: .default) { _ in
          self.pushThread(for: indexPath, page: .firstPage)
        }
        let lastPageAction = UIAlertAction(title: "la dernière page", style: .default) { _ in
          self.pushThread(for: indexPath, page: .lastPage)
        }
        let lastPostAction = UIAlertAction(title: "la dernière réponse", style: .default) { _ in
          self.pushThread(for: indexPath, page: .lastPost)
        }

        alertController.addAction(firstPageAction)
        alertController.addAction(lastPageAction)
        alertController.addAction(lastPostAction)

        if let lastPage = thread.lastPage {
          let pageSelectAction = UIAlertAction(title: "la page numéro...", style: .default) { _ in

            let pageNumberAlert = UIAlertController(title: "Aller à la page...", message: nil, preferredStyle: .alert)

            let confirmAction = UIAlertAction(title: "Confirmer", style: .default) { [weak pageNumberAlert] _ in

              let pageTextField = pageNumberAlert?.textFields![0]
              self.pushThread(for: indexPath, page: .pageNumber(num: (pageTextField?.text)!))
            }
            confirmAction.isEnabled = false

            let cancelAction = UIAlertAction(title: "Annuler", style: .cancel) { action in
              print(action)
            }
            pageNumberAlert.addTextField { textField in
              textField.placeholder = "(numéro entre 1 et \(lastPage)"
              textField.textAlignment = .center
              textField.keyboardType = .numberPad
              NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: OperationQueue.main) { _ in
                //FIX <Thread> should check if number is valid
                if let text = textField.text {
                  if let num = Int(text), let maxPage = Int(lastPage) {
                    if num < 1 {
                      textField.text = "1"
                      confirmAction.isEnabled = true
                      return
                    }
                    if num > maxPage {
                      textField.text = lastPage
                      confirmAction.isEnabled = true
                      return
                    }
                    if num >= 1 && num <= maxPage {
                      confirmAction.isEnabled = true
                      return
                    }
                  }
                }

                confirmAction.isEnabled = false
                return

              }
            }
            pageNumberAlert.addAction(cancelAction)
            pageNumberAlert.addAction(confirmAction)

            self.present(pageNumberAlert, animated: true)
          }

          alertController.addAction(pageSelectAction)

        }

        let removeStarredAction = UIAlertAction(title: "Supprimer des Favoris", style: .destructive) { _ in
          //FIXME Changer message vers Lus/Suivis en fonction du type de flag affichés
          //FIXME Message d'attente
          //FIXME Creer AlertView Generique
          thread.removeStarred(withForm: self.formInputs).done { _ in
            let alert = UIAlertController(title: "Hooray !", message: "Favori supprimé avec succès", preferredStyle: .alert)
            self.present(alert, animated: true) {
              self.fakePull()
              DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                alert.dismiss(animated: true)
              }
            }
          }.catch { _ in
            let alert = UIAlertController(title: "Ooops :(",
                                          message: "Problème survenu lors de la suppression.\nActualisez la liste et réessayez.",
                                          preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "D'accord !", style: .cancel))
            self.present(alert, animated: true)
          }
        }
        let copyLinkAction = UIAlertAction(title: "Copier le lien", style: .default) { _ in
          //print(thread.pastableURL)
          UIPasteboard.general.string = thread.pastableURL

          let alert = UIAlertController(title: "Hooray !", message: "Lien copié avec succès", preferredStyle: .alert)
          self.present(alert, animated: true) {

            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
              alert.dismiss(animated: true)
            }
          }
        }
        let cancelAction = UIAlertAction(title: "Annuler", style: .cancel) { _ in }

        alertController.addAction(copyLinkAction)
        //FIXME Gerer dans les MP/Sections
        if !thread.starredInput.isEmpty && !(self.formInputs?.isEmpty)! && listType == .starred {
          alertController.addAction(removeStarredAction)
        }
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true) {
          // ...
        }
      }
    }
  }

  func pushThread(for indexPath: IndexPath, page: ThreadPageType) {
    let thread = self.threads[sections[indexPath.section].threadIDs[indexPath.row]]!
    thread.read = true
    self.tableView.reloadRows(at: [indexPath], with: .none)
    
    self.pushThread(thread: thread, initialPageType: page)
  }
  func pushThread(thread: Thread, initialPageType: ThreadPageType) {
    self.delegate?.selectThread(thread, initialPageType: initialPageType)
  }
  var dropDownTitle: DropDownTitleView?

  func configure(forNewFilter filter: SectionFilter) {
    self.currentFilter = filter
    dropDownTitle?.updateSubtitle(newSubtitle: self.currentFilter.shortDescription)
    //self.configureToolBar()
    self.fakePull()
  }
  func configureTitleView(with title: String) {
    dropDownTitle = DropDownTitleView(title: title, subtitle: self.currentFilter.shortDescription)
    dropDownTitle?.delegate = self
    self.navigationItem.titleView = dropDownTitle!
  }
  private func configure(forNewListType listType: ThreadType, resetFilter: Bool = false) {
    print("Configure for \(listType)")
    self.listType = listType
    self.lastLoadedPage = nil

    switch self.listType {
    case .section:
      self.configureTitleView(with: selectedSection!.name)
      self.configure(forNewFilter: ((resetFilter == true) ? self.listType.defaultFilter() : self.currentFilter))
    case .starred:
      self.configureTitleView(with: "Sujets")
      self.configure(forNewFilter: self.listType.defaultFilter())
    case .message:
      self.configureTitleView(with: "Messages")
      self.configure(forNewFilter: self.listType.defaultFilter())
    }



  }

  @objc func fakePull() {
    DispatchQueue.main.async {
      if self.listType != .starred {
        self.tableView.enableInfiniteScrolling()
      } else {
        self.tableView.disableInfiniteScrolling()
      }
      self.tableView.triggerPullToRefresh()
    }
  }

  private func configureNavigationBar() {

    //UIBarButtonItem(title: "BUG", style: .done, target: self, action: #selector(showREADME)),
    /*
 UIBarButtonItem(fontAwesomeName: "fa-bug",
 target: self,
 action: #selector(showBUGS),
 location: .navigationBar),
 */
    self.navigationItem.leftBarButtonItems = [UIBarButtonItem(fontAwesomeName: "fa-hand-lizard-o",
                                                              target: self,
                                                              action: #selector(showCHANGES),
                                                              location: .navigationBar)]

    self.navigationItem.rightBarButtonItem = UIBarButtonItem(fontAwesomeName: "fa-user-circle",
                                                             target: self,
                                                             action: #selector(showUserProfil),
                                                             location: .navigationBar)

    self.navigationItem.rightBarButtonItem?.isEnabled = false

  }

  @objc func showBUGS() {
    let md = MarkDownViewController(bundledMarkdown: "BUGS")
    self.present(UINavigationController(rootViewController: md), animated: true, completion: nil)
  }
  @objc func showCHANGES() {
    //print("BUGBUGBUGBUGBUG")

    //self.pushThread(thread: Thread(url: "/hfr/Discussions/Sports/football-ballon-rond-sujet_61179_48461.htm"), initialPageType: .current)
    ///hfr/apple/unique-hfr-dispo-sujet_1711_365.htm
    let md = MarkDownViewController(bundledMarkdown: "CHANGES")
    self.present(UINavigationController(rootViewController: md), animated: true, completion: nil)

  }

  @objc func showLOGS() {
    let md = MarkDownViewController(bundledMarkdown: "LOGS")
    self.present(UINavigationController(rootViewController: md), animated: true, completion: nil)
  }

  deinit {
    print("DEALLOC ThreadsList VC")
  }
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    //print("TLVC didReceiveMemoryWarning didReceiveMemoryWarning")

    // Dispose of any resources that can be recreated.
  }
/*
  private func configureToolBar() {
    print("CONFIGURE BAR")
    let flexibleSpaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let fixedSpaceItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
    fixedSpaceItem.width = 10

    //let sectionsItem = UIBarButtonItem(fontAwesomeName: "fa-folder-open-o", target: self, action: #selector(showSectionsPicker(sender:)))
    //let starredItem = UIBarButtonItem(fontAwesomeName: "fa-star-o", target: self, action: #selector(loadStarredThreads))
    //let messagesItem = UIBarButtonItem(fontAwesomeName: "fa-envelope-o", target: self, action: #selector(loadMessages))
    let composeItem = UIBarButtonItem(fontAwesomeName: "fa-pencil-square-o", target: self, action: #selector(showComposer))

    composeItem.isEnabled = false

    let filterButton = UIButton(type: .system)
    var filterAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12, weight: .heavy)] as [NSAttributedStringKey: Any]

    filterAttributes[NSAttributedStringKey.foregroundColor] = UIColor.black

    if self.listType.filters().count == 1 {
      filterAttributes[NSAttributedStringKey.foregroundColor] = UIColor.black
    } else {
      filterButton.addTarget(self, action: #selector(showFilterPicker(sender:)), for: .touchUpInside)
      filterAttributes[NSAttributedStringKey.foregroundColor] = UIColor(red: 36/255, green: 112/255, blue: 216/255, alpha: 1)
    }

    let prefixAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11, weight: .light),
                            NSAttributedStringKey.foregroundColor: UIColor.black] as [NSAttributedStringKey: Any]

    let prefix: NSMutableAttributedString = NSMutableAttributedString(string: "Filtre : ", attributes: prefixAttributes)
    let filter: NSAttributedString = NSAttributedString(string: self.currentFilter.shortDescription, attributes: filterAttributes)

    prefix.append(filter)

//    statusLabel.backgroundColor = .clear

    filterButton.setAttributedTitle(prefix, for: .normal)
    filterButton.titleLabel?.numberOfLines = 2
    filterButton.titleLabel?.sizeToFit()
    filterButton.titleLabel?.textAlignment = .center
    filterButton.sizeToFit()

    let statusLabelItem = UIBarButtonItem(customView: filterButton)

    self.navigationController?.setToolbarHidden(false, animated: false)
    self.navigationController?.toolbar.isTranslucent = false

    self.setToolbarItems([//sectionsItem, starredItem, messagesItem,
                          flexibleSpaceItem, statusLabelItem, flexibleSpaceItem,
                          composeItem], animated: false)

  }
*/
  @objc func showUserProfil() {
    print("showUserProfil")
    //self.showLoginForm()
    
    if UIApplication.shared.alternateIconName == nil {
      UIApplication.shared.setAlternateIconName("Icon-RED")
    } else if UIApplication.shared.alternateIconName == "Icon-RED" {
      UIApplication.shared.setAlternateIconName("Icon-Original")
    } else if UIApplication.shared.alternateIconName == "Icon-Original" {
      UIApplication.shared.setAlternateIconName(nil)
    }
  }
/*
  @objc func showFilterPicker(sender: UIButton) {

    let sc = FilterListViewController(listType: self.listType, currentFilter: self.currentFilter)
    sc.delegate = self
    let nc = UINavigationController(rootViewController: sc)
    nc.modalPresentationStyle = .popover
    sc.preferredContentSize = CGSize(width: 220, height: 100)

    if let presentation = nc.popoverPresentationController {
      var rect = self.navigationController?.toolbar.frame
      rect?.origin.y -= 70
      presentation.sourceRect = rect!
      presentation.sourceView = self.view
      presentation.canOverlapSourceViewRect = false

      presentation.delegate = self
    }

    self.present(nc, animated: true, completion: nil)
  }
  */
  func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
    print("adaptivePresentationStyle \(controller)")
    return UIModalPresentationStyle.none
  }
/*
  @objc func showSectionsPicker(sender: UIBarButtonItem) {

    let sc = SectionListViewController()
    sc.delegate = self
    let nc = UINavigationController(rootViewController: sc)
    nc.modalPresentationStyle = .popover
    //sc.preferredContentSize = CGSize(width: 500, height: 600)

    if let presentation = nc.popoverPresentationController {
      var rect = self.navigationController?.toolbar.frame
      rect?.origin.y -= 70
      presentation.sourceRect = rect!
      presentation.sourceView = self.view
      presentation.canOverlapSourceViewRect = false

      presentation.delegate = self
    }

    self.present(nc, animated: true, completion: nil)
  }
 */
  @objc func showComposer() {
    print("showComposer")
  }
  @objc func loadStarredThreads() {
    //print("loadStarredThreads")
    self.configure(forNewListType: .starred)
    self.selectedSection = nil
  }
  @objc func loadMessages() {
    //print("loadMessages")
    self.configure(forNewListType: .message)
    self.selectedSection = nil
  }

  @objc func refreshData() {
    print("refreshData...")

    //FIXME Remplacer par MDN.client.getThreadFor(self.listType)
    //FIXME Creer Custom Struct pour results ([ThreadSection], [[ThreadID: Thread]], FormInputs)
    switch self.listType {
    case .starred:
      MDN.client.getStarredThreads(filter: self.currentFilter).done { results in
        self.handleSuccess(results)
      }.catch { error in
        self.handleError(error)
      }
    case .section:
      MDN.client.getThreads(for: self.selectedSection!, filter: self.currentFilter).done { results in
        self.handleSuccess(results)
      }.catch { error in
        self.handleError(error)
      }
    case .message:
      MDN.client.getMessages().done { results in
        self.handleSuccess(results)
      }.catch { error in
        self.handleError(error)
      }
    }
  }

  func loadNextPage() {
    if self.listType == .message, let lastPage = self.lastLoadedPage {
      MDN.client.getMessages(forPage: "\(Int(lastPage)!+1)").done { results in
        self.handleSuccess(results)
        }.catch { error in
          self.handleError(error)
      }
    } else if self.listType == .section, let lastPage = self.lastLoadedPage {
      MDN.client.getThreads(for: self.selectedSection!, filter: self.currentFilter, forPage: "\(Int(lastPage)!+1)").done { results in
        self.handleSuccess(results)
        }.catch { error in
          self.handleError(error)
      }
    }

  }

  func handleSuccess(_ results: ThreadsResults) {
    print("handleSuccess...")
    self.tableView.tableHeaderView = nil

    self.formInputs?.removeAll()

    if self.lastLoadedPage == nil {
      self.sections.removeAll(keepingCapacity: false)
      self.threads.removeAll(keepingCapacity: false)
      self.tableView.reloadData()
    }

    if self.listType != .starred {
      self.lastLoadedPage = results.sections.first?.name.replacingOccurrences(of: "Page ", with: "")
    }

    if self.lastLoadedPage == "1" {
      self.sections.removeAll(keepingCapacity: false)
      self.threads.removeAll(keepingCapacity: false)
      self.tableView.reloadData()
    }


    results.sections.forEach { dic in
      self.sections.append(dic)
    }

    results.threads.forEach { dic in
      dic.forEach { id, thread in
        self.threads[id] = thread
      }
    }

    self.formInputs = results.form

    if let updatedSection = results.section {
      print("We need to update the current section !")
      //print(updatedSection)
      self.selectedSection?.updating(with: updatedSection)
    }

    self.tableView.pullToRefreshView.stopAnimating()
    self.tableView.infiniteScrollingView.stopAnimating()

    self.tableView.reloadData()
    if self.lastLoadedPage == "1" {
      print("First Page > To TOP")
      self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
  }

  func handleError(_ error: Error) {
    //print("handleError...")
    self.tableView.pullToRefreshView.stopAnimating()
    self.tableView.infiniteScrollingView.stopAnimating()

    if self.listType == .starred || self.lastLoadedPage == nil {
      self.sections.removeAll(keepingCapacity: false)
      self.threads.removeAll(keepingCapacity: false)
      self.tableView.reloadData()
    }

    switch error as! MDNException {
      case .authNeeded:
        print("Auth Needed")
        self.tableView.tableHeaderView = LoginErrorView {
          let lvc = LoginFormViewController()
          lvc.delegate = self
          self.present(UINavigationController(rootViewController: lvc), animated: true, completion: nil)
        }
      case .empty:
        print("noThreads")
        self.tableView.tableHeaderView = EmptyListErrorView {
          self.fakePull()
        }
      case .networkFailure(let error):
        print(error.localizedDescription)
        self.tableView.tableHeaderView = NetworkFailureErrorView(message: error.localizedDescription) {
          self.fakePull()
        }
      case .parsing(let msg):
        print(msg)
        self.tableView.tableHeaderView = UnknownErrorView {
          self.fakePull()
        }
      default:
        print("Wot?")
        self.tableView.tableHeaderView = UnknownErrorView {
          self.fakePull()
        }
    }

    self.tableView.tableHeaderView?.snp.makeConstraints({ make in
      make.size.equalTo(self.view)
    })

    self.tableView.setNeedsLayout()
    self.tableView.layoutIfNeeded()
  }

  override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
    //print("traitCollectionDidChange traitCollectionDidChange \(previousTraitCollection?.verticalSizeClass.rawValue)")

    self.tableView.setNeedsLayout()
    self.tableView.layoutIfNeeded()
  }

  @objc func showLoginForm() {
    print("showLoginForm")
    let lvc = LoginFormViewController()
    lvc.delegate = self
    self.present(UINavigationController(rootViewController: lvc), animated: true, completion: nil)

  }
}

// MARK: - UITableViewDataSource
extension ThreadsListViewController: UITableViewDataSource {

  func numberOfSections(in tableView: UITableView) -> Int {
    //print("SECTIONS = \(sections.count)")
    return sections.count
  }
/*
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return sections[section].name.uppercased()
  }
*/
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    //print("viewForHeaderInSection \(section)")

    let headerView = UIView()
    headerView.backgroundColor = UIColor(red: 239/255.0, green: 239/255.0, blue: 244/255.0, alpha: 0.8)
    let headerButton = SectionHeaderButton(frame: .zero)

    let style = NSMutableParagraphStyle()
    style.alignment = .left

    let titleAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .light),
                                                         NSAttributedString.Key.foregroundColor: UIColor.darkGray,
                                                         NSAttributedString.Key.paragraphStyle: style]

    let title = NSMutableAttributedString(string: sections[section].name.uppercased(),
                                          attributes: titleAttributes)

    if let section = sections[section].section {
      let suffixAttributes: [NSAttributedString.Key : Any] = [NSAttributedString.Key.font: UIFont.fontAwesome(ofSize: 6),
                                                             NSAttributedString.Key.foregroundColor: UIColor.darkGray,
                                                             NSAttributedString.Key.paragraphStyle: style,
                                                             NSAttributedString.Key.baselineOffset: 2]

      let suffix = NSMutableAttributedString(string: "  " + String.fontAwesomeIcon(code: "fa-chevron-right")!,
                                             attributes: suffixAttributes)

      title.append(suffix)
      headerButton.section = section
      headerButton.addTarget(self, action: #selector(loadSection), for: UIControl.Event.touchUpInside)
    } else {
      headerButton.isEnabled = false
    }

    headerButton.addTarget(self, action: #selector(touchSection(button:event:)), for: UIControl.Event.allTouchEvents)
    headerButton.setAttributedTitle(title, for: UIControl.State.normal)
    headerButton.contentHorizontalAlignment = .left

    headerView.addSubview(headerButton)

    headerButton.snp.makeConstraints { make in
      make.edges.equalToSuperview().inset(UIEdgeInsets(top: 4, left: 13, bottom: 4, right: 10))
    }

    return headerView
  }


/*
   Disabling the longpress gesture when touching a section header
 */
  @objc func touchSection(button: SectionHeaderButton, event: UIEvent) {

    switch event.allTouches?.first?.phase {
    case .ended?:
      longPress.isEnabled = true
    default:
      longPress.isEnabled = false

    }

  }

  @objc func loadSection(button: SectionHeaderButton) {
    if let section = button.section {
      //self.delegate?.selectSection(
      self.delegate?.selectSection(section, filter: self.currentFilter)
      //self.selectedSection = section
      //self.configure(forNewListType: .section)
    }
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    //print("ROW in \(section) = \(sections[section].threadIDs.count)")
    return sections[section].threadIDs.count
    /*
    if sections[section].threadIDs.count > 1 {
      return 1
    } else {
      return sections[section].threadIDs.count
    }
     */
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "threadCell", for: indexPath) as! ThreadTableViewCell

    if let thread = self.threads[sections[indexPath.section].threadIDs[indexPath.row]] {
      cell.configure(with: thread)
    } else {
      cell.nameLabel.text = "): (*&*&^$#^(#*(_)(!#*&!*#&^*!&@#)"
    }


    return cell
  }

}

// MARK: - UITableViewDelegate
extension ThreadsListViewController: UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.tableView.deselectRow(at: indexPath, animated: true)
    self.pushThread(for: indexPath, page:self.listType.defaultPageType())
  }

}

// MARK: - UIPopoverPresentationControllerDelegate
extension ThreadsListViewController: UIPopoverPresentationControllerDelegate {

}

// MARK: - SwipablePanel
extension ThreadsListViewController: SwipablePanel {
  func enableScrolling() {
    self.tableView.panGestureRecognizer.isEnabled = true
  }
  func disableScrolling() {
    self.tableView.panGestureRecognizer.isEnabled = false
  }

}

// MARK: - AuthenticationDelegate
extension ThreadsListViewController: AuthenticationDelegate {
  func logged() {
    self.fakePull()
    self.dismiss(animated: true, completion: nil)
  }
}

// MARK: - SectionListViewDelegate
extension ThreadsListViewController: ThreadsListDelegate {
  func selectSection(_ section: Section) {
    //print(section)
    self.dismiss(animated: true, completion: nil)
    self.selectedSection = section
    self.configure(forNewListType: .section, resetFilter: true)
  }
}

extension ThreadsListViewController: FilterListViewDelegate {
  func selectFilter(_ filter: SectionFilter) {
    //FIXME Rename SectionFilter to ThreadsFilter
    self.dismiss(animated: true, completion: nil)
    print(filter)
    self.configure(forNewFilter: filter)

  }
}

// MARK: - DropDownTitleDelegate
extension ThreadsListViewController: DropDownTitleDelegate {
  func shouldShowFilterPicker(from sourceView: UIView) {
    print("shouldShowFilterPicker")
    let sc = FilterListViewController(listType: self.listType, currentFilter: self.currentFilter)
    sc.delegate = self
    let nc = UINavigationController(rootViewController: sc)
    nc.modalPresentationStyle = .popover
    sc.preferredContentSize = CGSize(width: 220, height: 100)

    if let presentation = nc.popoverPresentationController {
      var rect = sourceView.frame
      rect.size.height = 0
      //print("old frame = \(sourceView.frame)")
      //print("New frame = \(self.view.convert(sourceView.superview!.frame, to: self.view))")
      presentation.sourceRect = rect//self.view.convert(sourceView.superview!.frame, to: self.view)
      presentation.sourceView = self.view
      presentation.canOverlapSourceViewRect = false
      presentation.backgroundColor = .white
      presentation.delegate = self
    }

    self.present(nc, animated: true, completion: nil)
  }
}
