//
//  PostFormViewController.swift
//  Infinity
//
//  Created by FLK on 21/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class PostFormViewController: UIViewController {

  weak var delegate: ThreadViewControllerDelegate?

  let url: String
  let mode: FormMode
  
  // Strings for cancel button, confirmation message, and send button
  let cancelTitle: String
  let sendTitle: String
  let sendMessage: String
  let alertMessage: String
  

  var formInputs = [String: String]()
  var baseSmileys: [Smiley]?
  var starredSmileys: [Smiley]?

  var fastAnswer: String?

  var smileysViewController: SmileysViewController?
  
  // MARK: - ViewOutlets
  weak var postFormView: PostFormView! { return self.view as! PostFormView }

  var customInput = PostFormAccessoryView(frame: CGRect(x: 0, y: 0, width: 0, height: 50))

  init(for mode: FormMode, with url: String) {
    self.url = url
    self.mode = mode
    switch mode {
      case .creation:
        self.cancelTitle = "Annuler"
        self.sendTitle = "Répondre"
        self.sendMessage = "Envoi"
        self.alertMessage = "Le contenu du message sera perdu"
      case .edition:
        self.cancelTitle = "Annuler"
        self.sendTitle = "Éditer"
        self.sendMessage = "Édition"
        self.alertMessage = "Les modifications seront perdues"
      case .deletion:
        self.cancelTitle = "Annuler"
        self.sendTitle = "Supprimer"
        self.sendMessage = "Suppression"
        self.alertMessage = "Confirmer la suppression ?"
    case .newMessage:
      self.cancelTitle = "Annuler"
      self.sendTitle = "Envoyer"
      self.sendMessage = "Envoi"
      self.alertMessage = "Le contenu du message sera perdu"
    }
    
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func loadView() {
    print("loadView")
    view = PostFormView(for: self.mode)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.configure()
    
    MDN.client.getFullFormInputs(with: url).done { results in
      self.formInputs = results.inputs
      self.baseSmileys = results.baseSmileys
      self.starredSmileys = results.starredSmileys

      self.postFormView.bodyTextView.text = self.formInputs["content_form"]
      if let fast = self.fastAnswer {
        self.postFormView.bodyTextView.insertText("\(fast)")
      }
    }.catch { error in
        print(error)
    }
  }

  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.postFormView.bodyTextView.becomeFirstResponder()
  }

  func configure() {
    self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: self.cancelTitle,
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(PostFormViewController.cancelConfirmation)
    )
    
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: self.sendTitle,
                                                             style: .done,
                                                             target: self,
                                                             action: #selector(PostFormViewController.sendConfirmation)
    )

    if mode == .deletion {
      self.postFormView.active = false
    } else {
      self.postFormView.active = true
      let notificationCenter = NotificationCenter.default
      notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
      notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
      
      self.postFormView.bodyTextView.inputAccessoryView = self.customInput
      self.customInput.formDelegate = self
      
      self.insertSmileysController()

    }
  }

  @objc func cancelConfirmation() {
    if self.navigationItem.rightBarButtonItem?.isEnabled == false {
      self.shouldHideSmilies()
    } else {
      if self.mode != .deletion && self.postFormView.bodyTextView.text.count > 0 { //FIX add isModified when textview is modified
        let alert = UIAlertController(title: "Attention !", message: self.alertMessage, preferredStyle: .alert)
        
        let confirmAction = UIAlertAction(title: "Confirmer", style: .destructive, handler: { _ in
          self.cancel()
        })
        alert.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: "Annuler", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true)
        
      } else {
        self.cancel()
      }
    }
  }
  
  func cancel() {
    UIApplication.shared.sendAction(NSSelectorFromString("resignFirstResponder"), to: nil, from: nil, for: nil)
    self.delegate?.cancel()
    self.dismiss(animated: true)
  }

  @objc func sendConfirmation() {
    if self.mode == .deletion {
      let alert = UIAlertController(title: "Attention !", message: self.alertMessage, preferredStyle: .alert)
      
      let confirmAction = UIAlertAction(title: "Confirmer", style: .destructive, handler: { _ in
        self.send()
      })
      alert.addAction(confirmAction)
      let cancelAction = UIAlertAction(title: "Annuler", style: .cancel, handler: nil)
      alert.addAction(cancelAction)
      
      self.present(alert, animated: true)
    } else {
      self.send()
    }
  }
  
  func send() {
    self.navigationItem.rightBarButtonItem?.isEnabled = false
    self.navigationItem.title = "\(self.sendMessage) en cours..."

    formInputs["content_form"] = self.postFormView.bodyTextView.text

    // Hacky, First responder resigning himself
    UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil)
    MDN.client.sendPostForm(for: mode, with: formInputs).done { result in // FIXME mode = .create,.edit,.delete
      usleep(200)
      print("==== PFVC")
      print(result)

      let alert = UIAlertController(title: "Hooray !", message: result.message, preferredStyle: UIAlertController.Style.alert)
      self.present(alert, animated: true) {

        self.delegate?.refresh(withNewScrollPosition: result.anchor ?? "")

        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(100)) {
          self.delegate?.cancel()
          alert.dismiss(animated: true)
          self.dismiss(animated: true)
        }

      }
    }.ensure {
      self.navigationItem.rightBarButtonItem?.isEnabled = true
      self.navigationItem.title = ""
    }.catch { error in
      switch error as! MDNException {
      case .native(let msg):
        let alert = UIAlertController(title: "Ooops :(", message: msg, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
          print("Form > OK")
        })
        alert.addAction(UIAlertAction(title: "Ressayer", style: .cancel) { _ in
          print("Form > Ressayer")
          self.send()
        })
        self.present(alert, animated: true)
      default:
        print("ERROR")
        print(error)
      }
    }
  }

  @objc func adjustForKeyboard(notification: Notification) {
    print("adjustForKeyboard")
    let userInfo = notification.userInfo!

    let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
    let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)

    print("adjustForKeyboard \(keyboardScreenEndFrame)")
    print("adjustForKeyboard \(keyboardViewEndFrame)")
  
    var topInset:CGFloat = 0
    var bottomInset:CGFloat = 0
    if self.mode == .newMessage {
      topInset = 100
    }
    
    if notification.name == UIResponder.keyboardWillHideNotification {
      bottomInset = 0
    } else {
      bottomInset = keyboardViewEndFrame.height
    }

    postFormView.bodyTextView.contentInset = UIEdgeInsets(top: topInset, left: 0, bottom: bottomInset, right: 0)
    postFormView.bodyTextView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: bottomInset, right: 0)

    let selectedRange = postFormView.bodyTextView.selectedRange
    postFormView.bodyTextView.scrollRangeToVisible(selectedRange)
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }


  var currentBBCode: BBCode?
  
  func insertSmileysController() {
    if smileysViewController == nil {
      smileysViewController = SmileysViewController(sections: [SmileySection(name: "Smilies", smileys: baseSmileys),
                                                               SmileySection(name: "Favoris", smileys: starredSmileys)])

      smileysViewController?.delegate = self
      self.smileysViewController?.view.alpha = 0
      self.addChild(smileysViewController!)
      smileysViewController?.didMove(toParent: self)

      self.view.addSubview(smileysViewController!.view)

      smileysViewController?.view.snp.makeConstraints { make in
        make.edges.equalToSuperview()
      }
    }
  }

}

extension PostFormViewController: SmileyViewDelegate {
  func didSelectSmiley(smiley: Smiley) {

    postFormView.bodyTextView.insertText(" \(smiley.code) ")
    self.shouldHideSmilies()

  }

}

extension PostFormViewController: BBCodeViewDelegate {

  func didSelect(code: BBCode) {
    print("didSelect \(code)")

    self.currentBBCode = code
    self.customInput.bbCodeItem.activated = postFormView.bodyTextView.insert(bbcode: code)
    self.dismiss(animated: true)
  }

}

// MARK: - UIPopoverPresentationControllerDelegate
extension PostFormViewController: UIPopoverPresentationControllerDelegate {
  func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
    //print("adaptivePresentationStyle \(controller)")
    return UIModalPresentationStyle.none
  }
}

extension PostFormViewController: PostFormAccessoryDelegate {

  func shouldCompleteBBCode() {
    //print("shouldCompleteBBCode")
    if let curCode = self.currentBBCode {
      self.customInput.bbCodeItem.activated = postFormView.bodyTextView.insert(bbcode: curCode, codeCompleteOnly: true)
      self.currentBBCode = nil
    }

    self.customInput.bbCodeItem.activated = false
  }

  @objc func shouldHideBBCodes() {
    if self.presentedViewController != nil {
      self.dismiss(animated: false)
    }
  }

  @objc func shouldShowBBCodes() {

    let sc = BBCodeViewController()
    sc.delegate = self
    let nc = UINavigationController(rootViewController: sc)
    nc.modalPresentationStyle = .popover
    sc.preferredContentSize = CGSize(width: 130, height: 90)

    if let presentation = nc.popoverPresentationController {
      var rect = self.postFormView.bodyTextView.inputAccessoryView?.frame
      let origin = self.postFormView.bodyTextView.inputAccessoryView?.convert((rect?.origin)!, to: self.view)

      rect?.origin = origin!
      rect?.origin.x = 90
      rect?.size.width = 90

      presentation.sourceRect = rect!
      presentation.sourceView = self.view
      presentation.canOverlapSourceViewRect = false
      presentation.backgroundColor = .white
      presentation.delegate = self
    }

    self.customInput.searchTextField.resignFirstResponder()
    self.present(nc, animated: true, completion: nil)


  }

  func shouldSearchSmilies(_ keyword: String) {
    MDN.client.getSmilies(for: keyword).done { results in
      //print("Get Smilies OK.... loading \(results)")
      self.shouldShowSmilies(sections: [SmileySection(name: "\(results.count) smiley\(results.count > 1 ? "s" : "") pour \"\(keyword)\"", smileys: results, type: .search)])
    }.catch { error in
      //print("ERROR")
      print(error)
    }
  }

  func shouldShowDefaultSmilies() {

    self.shouldShowSmilies(sections: [SmileySection(name: "Smilies", smileys: self.baseSmileys),
                                      SmileySection(name: "Favoris", smileys: self.starredSmileys, type: .starred)])
  }

  func shouldHideSmilies() {
    UIView.animate(withDuration: 0.2, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
      self.smileysViewController?.view.alpha = 0
    })

    let selectedRange = postFormView.bodyTextView.selectedRange
    postFormView.bodyTextView.scrollRangeToVisible(selectedRange)
    self.postFormView.bodyTextView.becomeFirstResponder()
    self.navigationItem.rightBarButtonItem?.isEnabled = true

  }

  func shouldShowSmilies(sections: [SmileySection]) {
    self.shouldHideBBCodes()

    self.smileysViewController?.sections.removeAll()
    self.smileysViewController?.collectionView.reloadData()

    UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil)
    UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil) // Feature :o
    UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil) // Feature #2 :o

    self.navigationItem.rightBarButtonItem?.isEnabled = false

    self.smileysViewController?.collectionView.contentOffset = .zero
    self.smileysViewController?.sections = sections
    self.smileysViewController?.collectionView.reloadData()


    UIView.animate(withDuration: 0.1, delay: 0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
      self.smileysViewController?.view.alpha = 1
    })
  }
}
