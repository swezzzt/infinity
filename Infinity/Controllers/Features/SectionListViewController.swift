//
//  SectionListViewController.swift
//  Infinity
//
//  Created by FLK on 24/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class SectionListViewController: UIViewController {
  var tableView = UITableView(frame: .zero, style: .plain)
  var sections = [Section]()
  var section: Section?

  weak var delegate: ThreadsListDelegate?

  override func viewDidLoad() {
    super.viewDidLoad()

    self.tableView.delegate = self
    self.tableView.dataSource = self
    //self.section?.name ?? "Catégories"

    let dropDownTitle = DropDownTitleView(title: self.section?.name ?? "Catégories", subtitle: "Toutes")
    //dropDownTitle?.delegate = self
    self.navigationItem.titleView = dropDownTitle

    self.tableView.register(SectionTableViewCell.self, forCellReuseIdentifier: "sectionCell")

    self.view.addSubview(self.tableView)
    self.tableView.snp.makeConstraints { make in
      make.size.equalTo(self.view)
    }
    //self.configureToolBar()
    //self.navigationController?.setToolbarHidden(false, animated: true)
    //self.navigationController?.toolbar.isTranslucent = false

    self.navigationItem.rightBarButtonItem = UIBarButtonItem(fontAwesomeName: "fa-user-circle",
                                                             target: self,
                                                             action: #selector(showUserProfil),
                                                             location: .navigationBar)
    
    if self.section != nil {
      self.navigationItem.rightBarButtonItem?.isEnabled = true
    } else {
      self.navigationItem.rightBarButtonItem?.isEnabled = false
    }
    self.refreshData()
  }

  func refreshData() {
    print("refreshData...")

    MDN.client.getSections(of: self.section).done { results in
      //print("Refreshed... \(results)")
      self.sections = results
      self.tableView.reloadData()
    } .catch { error in
      print(error)
    }
  }

  @objc func showUserProfil() {
    print("showUserProfil")
    //self.showLoginForm()
    
    if UIApplication.shared.alternateIconName == nil {
      UIApplication.shared.setAlternateIconName("Icon-RED")
    } else if UIApplication.shared.alternateIconName == "Icon-RED" {
      UIApplication.shared.setAlternateIconName("Icon-Original")
    } else if UIApplication.shared.alternateIconName == "Icon-Original" {
      UIApplication.shared.setAlternateIconName(nil)
    }
  }
  deinit {
    print("DEALLOC SectionList VC")
  }
/*
  private func configureToolBar() {
    print("CONFIGURE BAR")
    let flexibleSpaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
    let fixedSpaceItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
    fixedSpaceItem.width = 10

    let filterButton = UIButton(type: .system)
    var filterAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12, weight: .heavy)] as [NSAttributedStringKey: Any]

    filterAttributes[NSAttributedStringKey.foregroundColor] = UIColor.black

    let prefixAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11, weight: .light),
                            NSAttributedStringKey.foregroundColor: UIColor.black] as [NSAttributedStringKey: Any]

    let prefix: NSMutableAttributedString = NSMutableAttributedString(string: "Filtre : ", attributes: prefixAttributes)
    let filter: NSAttributedString = NSAttributedString(string: "Toutes", attributes: filterAttributes)

    prefix.append(filter)
    //    statusLabel.backgroundColor = .clear

    filterButton.setAttributedTitle(prefix, for: .normal)
    filterButton.titleLabel?.numberOfLines = 2
    filterButton.titleLabel?.sizeToFit()
    filterButton.titleLabel?.textAlignment = .center
    filterButton.sizeToFit()

    let statusLabelItem = UIBarButtonItem(customView: filterButton)

    self.setToolbarItems([flexibleSpaceItem, statusLabelItem, flexibleSpaceItem], animated: false)

    /*
    if var toolbarFrame = self.navigationController?.toolbar.frame {
      print("ToolbarFrame")
      toolbarFrame.size.height -= 20
      toolbarFrame.origin.y += 20
      self.navigationController?.toolbar.frame = toolbarFrame
    }
    */
  }
  */
}

// MARK: - UITableViewDataSource
extension SectionListViewController: UITableViewDataSource {

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.sections.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = self.tableView.dequeueReusableCell(withIdentifier: "sectionCell", for: indexPath) as! SectionTableViewCell

    if self.section == nil {
      cell.delegate = self
    }

    let section = self.sections[indexPath.row]
    cell.configure(with: section)

    return cell
  }

}

// MARK: - UITableViewDelegate
extension SectionListViewController: UITableViewDelegate {

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    print("SectionListViewController didSelectRowAt \(indexPath)")
    self.tableView.deselectRow(at: indexPath, animated: true)
    let section = sections[indexPath.row]
    self.delegate?.selectSection(section, filter: .all)
  }

  @objc func sectionAccessoryTapped(button: UIButton) {
    if let indexPath = self.tableView.indexPathForView(view: button) {
      let section = sections[indexPath.row]
      let sc = SectionListViewController()
      sc.section = section
      sc.delegate = self.delegate
      self.navigationController?.pushViewController(sc, animated: true)
    }

  }

}

// MARK: - SwipablePanel
extension SectionListViewController: SwipablePanel {
  func enableScrolling() {
    self.tableView.panGestureRecognizer.isEnabled = true
  }
  func disableScrolling() {
    self.tableView.panGestureRecognizer.isEnabled = false
  }
}
