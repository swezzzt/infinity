//
//  UserProfileViewController.swift
//  Infinity
//
//  Created by FLK on 20/10/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController {
  
  let url: String
  
  init(with url: String) {
    self.url = url
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = .white

    let closeButton = UIBarButtonItem(fontAwesomeName: "fa-window-close",
                                      target: self,
                                      action: #selector(UserProfileViewController.close),
                                      location: .navigationBar)
    self.navigationItem.rightBarButtonItem = closeButton
  }
  
  @objc private func close() {
    self.dismiss(animated: true)
  }
}
