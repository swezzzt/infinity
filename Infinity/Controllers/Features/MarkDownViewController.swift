//
//  MarkDownViewController.swift
//  Infinity
//
//  Created by FLK on 24/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
import Down
import SnapKit

class MarkDownViewController: UIViewController {

  var fileName: String
  var downView: DownView?

  init(bundledMarkdown fileName: String) {
    self.fileName = fileName
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = .white
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Fermer",
                                                            style: .done,
                                                            target: self,
                                                            action: #selector(cancel))

    if let filepath = Bundle.main.path(forResource: self.fileName, ofType: "md") {
      do {
        let contents = try String(contentsOfFile: filepath)

        self.downView = try? DownView(frame: self.view.bounds, markdownString: contents) {
          self.view.addSubview(self.downView!)
          self.downView!.snp.makeConstraints { make in
            make.edges.equalTo(self.view)
          }

        }
      } catch {

      }
    }

    // Do any additional setup after loading the view.
  }

  @objc func cancel() {
    self.dismiss(animated: true, completion: nil)
  }

}
