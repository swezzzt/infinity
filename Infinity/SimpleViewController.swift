//
//  SimpleViewController.swift
//  Infinity
//
//  Created by FLK on 15/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class SimpleViewController: UIViewController {

  private var index: Int

  init(index: Int) {
    self.index = index


    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = self.randomUIColor(alpha: 1)
    // Do any additional setup after loading the view.
    self.title = "Tab"
    let label = UILabel(frame: .zero)
    label.text = "Tab = \(self.index)"
    label.sizeToFit()
    label.backgroundColor = .blue
    self.view.addSubview(label)
    label.snp.makeConstraints { make in
      make.top.equalToSuperview().offset(10)
      make.left.equalToSuperview().offset(10)
      make.width.equalTo(100)
      make.height.equalTo(30)
    }
    //var currentFrame = self.view.frame
    //currentFrame.origin.y = CGFloat(index) * 30.0

  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  func randomUIColor(alpha:CGFloat!) -> UIColor
  {
    return  UIColor.init(red:(CGFloat(arc4random_uniform(256))/255.0), green:CGFloat(arc4random_uniform(256))/255.0, blue: (CGFloat(arc4random_uniform(256))/255.0), alpha: alpha)
  }

  /*
   // MARK: - Navigation

   // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
   // Get the new view controller using segue.destinationViewController.
   // Pass the selected object to the new view controller.
   }
   */

}
