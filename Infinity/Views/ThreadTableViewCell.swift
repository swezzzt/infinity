//
//  ThreadTableViewCell.swift
//  Infinity
//
//  Created by FLK on 29/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
import SnapKit

class ThreadTableViewCell: UITableViewCell {

  // MARK: UI
  let nameLabel: UILabel = {
    let label = UILabel()
    return label
  }()

  let subtitleLabel: UILabel = {
    let label = UILabel()
    label.font = UIFont.systemFont(ofSize: 12, weight: .thin)
    label.textColor = .gray
    return label
  }()

  let lastAnswerLabel: UILabel = {
    let label = UILabel()
    label.textAlignment = .right
    label.font = UIFont.systemFont(ofSize: 12, weight: .regular)
    label.textColor = UIColor(red: 36/255, green: 112/255, blue: 216/255, alpha: 1)
    return label
  }()

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: .default, reuseIdentifier: reuseIdentifier)

    self.contentView.addSubview(self.nameLabel)
    nameLabel.snp.makeConstraints { make in
      make.leadingMargin.equalToSuperview().offset(5)
      make.trailingMargin.equalToSuperview().offset(-5)
      make.topMargin.equalToSuperview().offset(5)
      make.height.equalToSuperview().dividedBy(2)
    }

    self.contentView.addSubview(self.subtitleLabel)
    subtitleLabel.snp.makeConstraints { make in
      make.bottom.equalToSuperview().offset(-5)
      make.leadingMargin.equalTo(self.nameLabel)
      make.top.equalTo(self.nameLabel.snp.bottom)
      make.width.equalToSuperview().dividedBy(2)
    }

    self.contentView.addSubview(self.lastAnswerLabel)
    lastAnswerLabel.snp.makeConstraints { make in
      make.bottom.equalTo(self.subtitleLabel)
      make.top.equalTo(self.subtitleLabel)
      make.left.equalTo(self.subtitleLabel.snp.right)
      make.trailingMargin.equalTo(self.nameLabel)

    }
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func configure(with thread: Thread) {

    var prefixString = ""

    if thread.sticky {
      prefixString += String.fontAwesomeIcon(code: "fa-thumb-tack")!
      prefixString += " "
    }
    if thread.locked {
      prefixString += String.fontAwesomeIcon(code: "fa-lock")!
      prefixString += " "
    }

    let prefixAttributes = [NSAttributedString.Key.font: UIFont.fontAwesome(ofSize: 13),
                            NSAttributedString.Key.foregroundColor: UIColor.red] as [NSAttributedString.Key: Any]

    var titleAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .medium),
                           NSAttributedString.Key.foregroundColor: UIColor.black] as [NSAttributedString.Key: Any]

    if thread.read {
      titleAttributes[NSAttributedString.Key.font] = UIFont.systemFont(ofSize: 14, weight: .light)
    }

    let prefix: NSMutableAttributedString = NSMutableAttributedString(string: prefixString, attributes: prefixAttributes)
    let title: NSAttributedString = NSAttributedString(string: (thread.filteredName() ?? "").trimmingCharacters(in: .whitespaces), attributes: titleAttributes)

    prefix.append(title)

    nameLabel.attributedText = prefix

    var subtitle = ""

    switch thread.type {
    case .section:
      if let answersCount = thread.answersCount {
        subtitle += "↺ \(answersCount)"
      }
    case .starred:
      let lastPage = thread.pageNumber(for: .lastPage)

      if let readPage = thread.lastReadPage {
        subtitle += "⚑ \(readPage) / \(lastPage)"
      } else {
        subtitle += "★ \(lastPage) / \(lastPage)"
      }
    case .message:
      if let author = thread.author {
        subtitle += "↪︎ \(author)"
      }
    }

    subtitleLabel.text = subtitle

    var lastAnswer = ""

    let now = Date()
    let formatter = DateFormatter()
    formatter.dateFormat = "dd/MM/yyyy"
    let dateString = formatter.string(from: now)
    lastAnswer += "\(thread.lastPostAuthor ?? "") - "
    if dateString != thread.lastPostDay {
      lastAnswer += "\(thread.lastPostDay ?? "")"
    } else {
      lastAnswer += "\(thread.lastPostHour ?? "")"
    }

    lastAnswerLabel.text = lastAnswer
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
}
