//
//  BBCodeBarButtonItem.swift
//  Infinity
//
//  Created by FLK on 13/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class BBCodeBarButtonItem: UIBarButtonItem {
  private var _activated: Bool = false
  var activated: Bool {
    get {
      return _activated
    }
    set {
      if newValue {
        self.setImage(fontAwesomeName: "fa-hand-paper-o")
      } else {
        self.setImage(fontAwesomeName: "fa-hand-rock-o")

      }
      _activated = newValue
    }
  }

}
