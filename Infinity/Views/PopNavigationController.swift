//
//  PopNavigationController.swift
//
//  Created by FLK on 18/06/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class PopNavigationController : UINavigationController, UINavigationControllerDelegate, UIViewControllerTransitioningDelegate {
  var interactivePopTransition: UIPercentDrivenInteractiveTransition?
  var customPop = CustomPopTransition()
  override func viewDidLoad() {
    self.delegate = self
  }

  func navigationController(_ navigationController: UINavigationController,
                            willShow viewController: UIViewController,
                            animated: Bool) {
    addPanGesture(viewController: viewController)
  }

  func navigationController(_ navigationController: UINavigationController,
                            animationControllerFor operation: UINavigationController.Operation,
                            from fromVC: UIViewController,
                            to toVC: UIViewController)
    -> UIViewControllerAnimatedTransitioning? {

      if (operation == .pop) {
        return customPop
      }
      else {
        return nil
      }
  }

  func navigationController(_ navigationController: UINavigationController,
                            interactionControllerFor animationController: UIViewControllerAnimatedTransitioning)
    -> UIViewControllerInteractiveTransitioning? {

      if (animationController is CustomPopTransition) {
        return interactivePopTransition
      }
      else {
        return nil
      }

  }

  func addPanGesture(viewController: UIViewController) {
    let popRecognizer = UIPanGestureRecognizer(target: self,
                                               action: #selector(self.handlePanRecognizer(recognizer:)))

    popRecognizer.delegate = self
    viewController.view.addGestureRecognizer(popRecognizer)
  }

}

extension PopNavigationController: UIGestureRecognizerDelegate {
/*
  func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    //print("shouldRecognizeSimultaneouslyWith \(gestureRecognizer) with \(otherGestureRecognizer)")
    return true
  }
*/
  @objc func handlePanRecognizer(recognizer: UIPanGestureRecognizer) {
    //print("handlePanRecognizer \(self)")
    // Calculate how far the user has dragged across the view
    var progress = recognizer.translation(in: self.view).x / self.view.bounds.size.width
    progress = min(1, max(0, progress))
    if (recognizer.state == .began) {
      // Create a interactive transition and pop the view controller
      self.interactivePopTransition = UIPercentDrivenInteractiveTransition()
      self.popViewController(animated: true)
    }
    else if (recognizer.state == .changed) {
      // Update the interactive transition's progress
      interactivePopTransition?.update(progress)
    }
    else if (recognizer.state == .ended || recognizer.state == .cancelled) {
      // Finish or cancel the interactive transition
      if (progress > 0.25) {
        interactivePopTransition?.finish()
      }
      else {
        interactivePopTransition?.cancel()
      }
      interactivePopTransition = nil
    }
  }
}

