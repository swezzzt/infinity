//
//  PostFormAccessoryView.swift
//  Infinity
//
//  Created by FLK on 08/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

protocol PostFormAccessoryDelegate: NSObjectProtocol {
  func shouldShowDefaultSmilies()
  func shouldSearchSmilies(_ keyword: String)
  func shouldShowBBCodes()
  func shouldCompleteBBCode()
  func shouldHideBBCodes()
}

class PostFormAccessoryView: UIToolbar {
  weak var formDelegate: PostFormAccessoryDelegate?

  let bbCodeItem = BBCodeBarButtonItem(fontAwesomeName: "fa-hand-rock-o", target: self, action: #selector(showBBCode(sender:)))

  let searchTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Recherche Smiley"
    textField.textColor = .darkGray
    textField.keyboardType = .default
    textField.borderStyle = .roundedRect
    textField.keyboardAppearance = .default
    textField.returnKeyType = .search
    textField.clearButtonMode = .always
    textField.enablesReturnKeyAutomatically = true
    textField.autocorrectionType = .no
    textField.spellCheckingType = .no
    textField.autocapitalizationType = .none
    textField.font = UIFont.systemFont(ofSize: 14)
    return textField
  }()

  override init(frame: CGRect) {
    print("init toolBar")
    super.init(frame: frame)

    self.searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    self.searchTextField.delegate = self
    self.searchTextField.frame.size = CGSize(width: 150, height: 40)

    let fixed = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.fixedSpace, target: self, action: nil)
    let flexible = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil)
    fixed.width = 10


    let smiliesItem = UIBarButtonItem(fontAwesomeName: "fa-smile-o", target: self, action: #selector(showSmilies))
    let rehostItem = UIBarButtonItem(fontAwesomeName: "fa-picture-o", target: self, action: #selector(showSmilies))
    let cameraItem = UIBarButtonItem(fontAwesomeName: "fa-camera", target: self, action: #selector(showSmilies))

    let wikiSearchItem = UIBarButtonItem(customView: self.searchTextField)

    self.items = [cameraItem, rehostItem, smiliesItem, bbCodeItem, flexible, wikiSearchItem]

    if #available(iOS 11.0, *) {
      self.backgroundColor = .clear

      let frost = UIVisualEffectView(effect: UIBlurEffect(style: .light))
      self.insertSubview(frost, at: 0)

      frost.snp.makeConstraints { make in
        make.edges.equalToSuperview()
      }
    } else {
      self.backgroundColor = .white
    }
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func draw(_ rect: CGRect) {
    UIColor.white.withAlphaComponent(0).setFill()
  }

  @objc func textFieldDidChange(_ textField: UITextField) {
    //print("textFieldDidChange")
    //print(self.postTextField)

  }

  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    self.formDelegate?.shouldHideBBCodes()
    return true
  }

  @objc func showSmilies() {
    print("showSmilies")
    self.formDelegate?.shouldShowDefaultSmilies()
  }

  @objc func showBBCode(sender: UIBarButtonItem) {
    //print("showBBCode \(sender)")

    if !bbCodeItem.activated {
      self.formDelegate?.shouldShowBBCodes()
    } else {
      sender.setImage(fontAwesomeName: "fa-hand-rock-o")
      self.formDelegate?.shouldCompleteBBCode()
      bbCodeItem.activated = false
    }
    
  }

}
/*
class PostFormAccessoryView: UIView {

  //weak var delegate: PostFormAccessoryDelegate?


  override init(frame: CGRect) {
    super.init(frame: frame)
    self.backgroundColor = .white

    self.addSubview(self.searchTextField)
    searchTextField.snp.makeConstraints { make in
      make.edges.equalToSuperview().inset(UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5))
    }


    //self.postTextField.inputAccessoryView = self

  }


  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

}
*/
extension PostFormAccessoryView: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    //print("textFieldShouldReturn")
    if let text = textField.text {
      self.formDelegate?.shouldSearchSmilies(text)
    }

    return false
  }

  func textFieldDidEndEditing(_ textField: UITextField) {
    //print("textFieldDidEndEditing")
  }

  func textFieldDidBeginEditing(_ textField: UITextField) {
    //print("textFieldDidBeginEditing")

  }
}
