//
//  PostWebView.swift
//  Infinity
//
//  Created by FLK on 22/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
import WebKit

class LeakAvoider : NSObject, WKScriptMessageHandler {
  weak var delegate : WKScriptMessageHandler?
  init(delegate:WKScriptMessageHandler) {
    self.delegate = delegate
    super.init()
  }

  func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
    print("LeakAvoider userContentController")
    self.delegate?.userContentController(userContentController, didReceive: message)
  }
}

class PostWebView: WKWebView {
  var isCustomMenuVisible = false

  override init(frame: CGRect, configuration: WKWebViewConfiguration) {
    super.init(frame: frame, configuration: configuration)
    self.addObservers()
  }

  func addObservers() {
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(PostWebView.menuControllerWillHide(notification:)),
                                           name: UIMenuController.willHideMenuNotification,
                                           object: nil)
  }

  func removeObservers() {
    NotificationCenter.default.removeObserver(self, name: UIMenuController.willHideMenuNotification, object: nil)

  }
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  @objc func menuControllerWillHide(notification: Notification) {
    //print("menuControllerWillHide")
    /*
    guard self.isFirstResponder else {
      print("menuControllerWillHide not FR")
      return
    }
    
    */
    
    self.isCustomMenuVisible = false
    if let menuController = notification.object as? UIMenuController {
      print("menuControllerWillHide Reset menu Items")
      menuController.menuItems = []
    }
  }

  override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {

    if self.isCustomMenuVisible {
      //print("canPerformAction \(action) FALSE")
      return false
    } else {
      //print("canPerformAction \(action) SUPER")

      return super.canPerformAction(action, withSender: sender)
    }

  }

}
