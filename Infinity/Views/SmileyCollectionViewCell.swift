//
//  SmileyCollectionViewCell.swift
//  Infinity
//
//  Created by FLK on 10/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
import SDWebImage

class SmileyCollectionViewCell: UICollectionViewCell {

  // MARK: UI
  let smileyView: FLAnimatedImageView = {
    let imageView = FLAnimatedImageView()
    imageView.contentMode = .center
    //imageView.backgroundColor = .green
    return imageView
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    //self.contentView.backgroundColor = .red
    self.backgroundColor = .white
    self.contentView.addSubview(self.smileyView)
    self.smileyView.snp.makeConstraints { make in
      make.edges.equalToSuperview().inset(UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5))
    }
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func configure(with smiley: Smiley) {
    self.smileyView.contentMode = .center

    guard let url = URL(string: smiley.url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!) else {
      print("Wrong URL \(smiley.url)")
      return
    }

    let placeHolder = UIImage.fontAwesomeIcon(code: "fa-smile-o",
                                              textColor: .red,
                                              size: CGSize(width: 20, height: 20))

    self.smileyView.sd_setImage(with: url,
                                placeholderImage: placeHolder) { (image, error, cacheType, imageURL) in

                            if let img = image {
                              let size = self.smileyView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)

                              if img.size.width > size.width ||
                                img.size.height > size.height {
                                self.smileyView.contentMode = .scaleAspectFit
                              }
                            }
    }
/*
    var config = URLSessionConfiguration.default
    config.requestCachePolicy = .returnCacheDataElseLoad
    let session = URLSession(configuration: config)
    URLSession.shared.dataTask(with: url) { (data, response, error) in
      if error != nil {
        print("Failed fetching image: \(url)")
        DispatchQueue.main.async {
          self.smileyView.image = UIImage.fontAwesomeIcon(code: "fa-exclamation-triangle",
                                                          textColor: .red,
                                                          size: CGSize(width: 20, height: 20))
        }
        return
      }

      guard let response = response as? HTTPURLResponse, response.statusCode == 200 else {
        print("Not a proper HTTPURLResponse or statusCode \(url)")
        DispatchQueue.main.async {
          self.smileyView.image = UIImage.fontAwesomeIcon(code: "fa-exclamation-triangle",
                                                          textColor: .red,
                                                          size: CGSize(width: 20, height: 20))
        }
        return
      }

      DispatchQueue.main.async {
        self.smileyView.image = UIImage(data: data!)
        if self.smileyView.image!.size.width > self.smileyView.bounds.size.width ||
           self.smileyView.image!.size.height > self.smileyView.bounds.size.height {
          print("url \(url) changeContentMode")
          self.smileyView.contentMode = .scaleAspectFit
        } else {
          print("url \(url) dont changeContentMode")

        }
      }
    }.resume()
 */
  }
}
