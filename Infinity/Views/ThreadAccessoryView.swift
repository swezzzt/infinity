//
//  ThreadAccessoryView.swift
//  Infinity
//
//  Created by FLK on 31/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

protocol ThreadAccessoryDelegate: NSObjectProtocol {
  func shouldAcceptTextChanges(text: String)
  func shouldDismissTextChanges()
}

class ThreadAccessoryView: UIView {

  weak var delegate: ThreadAccessoryDelegate?
  // MARK: Variables
  let postTextField: UITextField = {
    let textField = UITextField()
    textField.placeholder = "Réponse rapide"
    textField.textColor = .darkGray
    textField.keyboardType = .default
    textField.borderStyle = .roundedRect
    textField.keyboardAppearance = .default
    textField.returnKeyType = .send
    textField.clearButtonMode = .always
    textField.enablesReturnKeyAutomatically = true
    textField.font = UIFont.systemFont(ofSize: 14)
    return textField
  }()
/*
  override var canBecomeFirstResponder: Bool {
    return true
  }

  override var inputAccessoryView: UIView? {
    return self
  }
*/
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.backgroundColor = .white

    self.addSubview(self.postTextField)
    postTextField.snp.makeConstraints { make in
      make.edges.equalToSuperview().inset(UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5))
    }

    self.postTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    self.postTextField.delegate = self
    //self.postTextField.inputAccessoryView = self

  }

  @objc func textFieldDidChange(_ textField: UITextField) {
    print("textFieldDidChange")
    //print(self.postTextField)

  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

}

extension ThreadAccessoryView: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    print("textFieldShouldReturn")
    if let text = textField.text {
      self.delegate?.shouldAcceptTextChanges(text: text)
    }
    return false
  }

  func textFieldDidEndEditing(_ textField: UITextField) {
    print("textFieldDidEndEditing")
  }

  func textFieldDidBeginEditing(_ textField: UITextField) {
    print("textFieldDidBeginEditing")

  }
}
