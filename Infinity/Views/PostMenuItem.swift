//
//  PostMenuItem.swift
//  Infinity
//
//  Created by FLK on 22/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class PostMenuItem: UIMenuItem {
  var postID: Int

  init(title: String, action: Selector, postID: Int) {
    self.postID = postID

    super.init(title: title, action: action)
  }

}
