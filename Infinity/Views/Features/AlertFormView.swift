//
//  AlertFormView.swift
//  Infinity
//
//  Created by FLK on 20/10/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class AlertFormView: UIView {
  // MARK: Variables
  let bodyTextView: UITextView = {
    let textView = UITextView()
    textView.textColor = .black
    textView.keyboardType = .default
    textView.autocapitalizationType = .sentences
    textView.autocorrectionType = .default
    textView.spellCheckingType = .default
    textView.keyboardAppearance = .default
    textView.keyboardDismissMode = .interactive
    textView.alwaysBounceVertical = true
    textView.font = UIFont.systemFont(ofSize: 14)
    textView.text = "Chargement..."
    textView.backgroundColor = .clear
    return textView
  }()
  
  let placeholderTextView: UITextView = {
    let textView = UITextView()
    textView.textColor = .gray
    textView.isSelectable = false
    textView.isUserInteractionEnabled = false
    textView.font = UIFont.systemFont(ofSize: 14)
    textView.backgroundColor = .white
    textView.isHidden = true
    textView.text = """
    Attention : le message que vous écrivez ici sera envoyé directement chez les modérateurs via message privé ou e-mail.
    
    Ce formulaire est destiné UNIQUEMENT à demander aux modérateurs de venir sur le sujet lorsqu'il y a un problème.
    
    Il ne sert pas à appeler à l'aide parce que personne ne répond à vos questions.
    Il ne sert pas non plus à ajouter un message sur le sujet, pour cela il y a le menu 'Répondre' (s'il est absent c'est que le sujet a été cloturé).
    """
    return textView
  }()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.backgroundColor = .white
   
    self.addSubview(self.placeholderTextView)
    placeholderTextView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
    
    self.addSubview(self.bodyTextView)
    bodyTextView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
    bodyTextView.delegate = self

  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension AlertFormView: UITextViewDelegate {
  func textViewDidChange(_ textView: UITextView) {
    self.placeholderTextView.isHidden = (textView.text.count > 0)
  }
}
