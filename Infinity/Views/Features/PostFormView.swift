//
//  PostFormView.swift
//  Infinity
//
//  Created by FLK on 21/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class PostFormView: UIView {

  var active: Bool = false {
    didSet {
      bodyTextView.isSelectable = active
      bodyTextView.textColor = active ? .black : .gray
    }
  }
  // MARK: Variables
  let recipientsTextField: UITextField = {
    let textField = UITextField()
    //textField.backgroundColor = .blue
    textField.textColor = .white
    textField.placeholder = "À"
    return textField
  }()

  let titleTextField: UITextField = {
    let textField = UITextField()
    //textField.backgroundColor = .yellow
    textField.textColor = .white
    textField.placeholder = "Sujet"
    return textField
  }()

  let bodyTextView: BBCodeTextView = {
    let textView = BBCodeTextView()
    //textView.backgroundColor = .green
    textView.keyboardType = .default
    textView.autocapitalizationType = .sentences
    textView.autocorrectionType = .default
    textView.spellCheckingType = .default
    textView.keyboardAppearance = .default
    textView.keyboardDismissMode = .interactive
    textView.alwaysBounceVertical = true
    textView.font = UIFont.systemFont(ofSize: 14)
    textView.text = "Chargement..."
    return textView
  }()
  
  init(for mode: FormMode) {
    print("init PostFormView for \(mode)")
    super.init(frame: .zero)
    self.backgroundColor = .white

    self.addSubview(self.bodyTextView)
    bodyTextView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
    bodyTextView.delegate = self
    
    if mode == .newMessage {
      let headerView = UIView(frame: .zero)
      //headerView.backgroundColor = .red
      headerView.addSubview(self.recipientsTextField)
      headerView.addSubview(self.titleTextField)
      self.bodyTextView.addSubview(headerView)
      headerView.snp.makeConstraints { make in
        make.centerX.equalToSuperview()
        //make.height.equalTo(50)
        //make.width.equalTo(100)
        make.top.equalTo(self.bodyTextView).offset(-100)
        make.left.right.equalTo(self.bodyTextView)
        //make.leading.equalTo(self.bodyTextView.snp.leadingMargin)
        //make.trailing.equalTo(self.bodyTextView.snp.trailingMargin)
        make.bottom.equalTo(titleTextField.snp.bottom)
      }
      
      recipientsTextField.snp.makeConstraints { make in
        make.height.equalTo(50)
        make.top.equalTo(self.bodyTextView).offset(-100)
        make.left.right.equalToSuperview()
        //make.topMargin.equalToSuperview()
        //make.leading.equalToSuperview()
        //make.trailing.equalToSuperview()
      }
      
      titleTextField.snp.makeConstraints { make in
        make.height.equalTo(50)
        make.top.equalTo(self.recipientsTextField.snp.bottom)
        make.left.right.equalTo(self.recipientsTextField)
        make.bottom.equalToSuperview()
      }
      
      self.bodyTextView.contentInset = UIEdgeInsets(top: 100, left: 0, bottom: 0, right: 0)
      //self.bodyTextView.contentOffset = CGPoint(x: 0, y: 100)
    }
/*
    recipientsTextField.snp.makeConstraints { make in
      make.height.equalTo(50)
    }
    titleTextField.snp.makeConstraints { make in
      make.height.equalTo(50)
    }
*/
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension PostFormView: UITextViewDelegate {
  func textViewDidBeginEditing(_ textView: UITextView) {
  }

  func textViewDidEndEditing(_ textView: UITextView) {
  }
}
