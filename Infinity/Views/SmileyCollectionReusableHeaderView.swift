//
//  SmileyCollectionReusableHeaderView.swift
//  Infinity
//
//  Created by FLK on 10/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

class SmileyCollectionReusableHeaderView: UICollectionReusableView {
  // MARK: UI
  let nameLabel: UILabel = {
    let label = UILabel()
    label.font = UIFont.systemFont(ofSize: 14, weight: .light)
    label.textColor = .darkGray
    label.textAlignment = .left
    label.numberOfLines = 0
    return label
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    self.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 0.8)

    self.addSubview(self.nameLabel)
    nameLabel.snp.makeConstraints { make in
      make.edges.equalToSuperview().inset(UIEdgeInsets(top: 9, left: 13, bottom: 9, right: 10))
    }
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func configure(with title: String) {
    self.nameLabel.text = title.uppercased()
    self.nameLabel.sizeToFit()
  }
}
