//
//  MDNExtensions.swift
//  Infinity
//
//  Created by FLK on 27/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import Foundation

extension String {
  
  // MARK: - SHA256
  func get_sha256_String() -> String {
    guard let data = self.data(using: .utf8) else {
      print("Data not available")
      return ""
    }
    return getHexString(fromData: digest(input: data as NSData))
  }
  
  private func digest(input : NSData) -> NSData {
    let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
    var hashValue = [UInt8](repeating: 0, count: digestLength)
    CC_SHA256(input.bytes, UInt32(input.length), &hashValue)
    return NSData(bytes: hashValue, length: digestLength)
  }
  
  private func getHexString(fromData data: NSData) -> String {
    var bytes = [UInt8](repeating: 0, count: data.length)
    data.getBytes(&bytes, length: data.length)
    
    var hexString = ""
    for byte in bytes {
      hexString += String(format:"%02x", UInt8(byte))
    }
    return hexString
  }
}

extension String {
  func decodeMDNEncryptedURL() -> String {
    var decryptedURL = ""

    let cryptedURL = self[self.index(self.startIndex, offsetBy: 20)...]
    let base16 = "0A12B34C56D78E9F"

    for i in stride(from: 0, to: cryptedURL.count, by: 2) where i+1 < cryptedURL.count {

      let chIndex = cryptedURL.index(cryptedURL.startIndex, offsetBy: i)
      let clIndex = cryptedURL.index(cryptedURL.startIndex, offsetBy: i+1)

      let chCharacter = cryptedURL[chIndex]
      let clCharacter = cryptedURL[clIndex]

      let chRange: Range<String.Index> = base16.range(of: String(chCharacter))!
      let clRange: Range<String.Index> = base16.range(of: String(clCharacter))!

      let chFinalIndex: Int = base16.distance(from: base16.startIndex, to: chRange.lowerBound)
      let clFinalIndex: Int = base16.distance(from: base16.startIndex, to: clRange.lowerBound)

      //print("\(i) \(chCharacter) = \(chFinalIndex) | \(clCharacter) = \(clFinalIndex) == \(chFinalIndex*16+clFinalIndex)")

      decryptedURL.append(Character(UnicodeScalar(chFinalIndex*16+clFinalIndex)!))

    }

    return decryptedURL
  }

  func firstMatch(forPattern pattern: String) -> String? {
    let regex = try! NSRegularExpression(pattern: pattern, options: [])
    var firstMatch: String?
    let results = regex.matches(in: self,
                                options: [],
                                range: NSRange(location: 0,
                                               length: self.count))

    if let result = results.first {
      firstMatch = (self as NSString).substring(with: result.range(at: 1))
    }

    return firstMatch
  }

  func regexMatch(forPattern pattern: String) -> [String]? {
    let regex = try! NSRegularExpression(pattern: pattern, options: [])

    let results = regex.matches(in: self,
                                options: [],
                                range: NSRange(location: 0,
                                               length: self.count))

    var matches: [String]?
    if let result = results.first {
      matches = [String]()
      for i in 1...(result.numberOfRanges-1) {
        matches?.append((self as NSString).substring(with: result.range(at: i)))
      }
    }

    return matches
  }

  func scrollPosition() -> String {
    print("Getting scroll Position")
    //FIXME "bottom if bottom"
    let hashIndex = self.range(of: "#t",
                               options: .backwards)

    if let startIndex = hashIndex?.lowerBound, let endIndex = hashIndex?.upperBound {
      let hashStartIndex = self.index(startIndex,
                                      offsetBy: self.distance(from: startIndex,
                                                              to: endIndex))

      return "post\(String(self[hashStartIndex..<self.endIndex]))"
    } else if self.range(of: "#bas") != nil {
      return "bottom"

    } else {
      return "top"
    }

  }
}
