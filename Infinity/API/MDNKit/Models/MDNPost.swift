//
//  MDNPost.swift
//  Infinity
//
//  Created by FLK on 16/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import Foundation
import PromiseKit

typealias PostID = String

class Post {
  var id: PostID
  var url: String?
  private var encodedEditURL: String?
  var editURL: String? {
    get {
      return encodedEditURL?.decodeMDNEncryptedURL()
    }
    set {
      self.encodedEditURL = newValue
    }
  }

  private var encodedQuoteURL: String?
  var quoteURL: String? {
    get {
      return encodedQuoteURL?.decodeMDNEncryptedURL()
    }
    set {
      self.encodedQuoteURL = newValue
    }
  }

  private var rawMultiQuoteJSString: String?
  var multiQuoteString: String? {
    get {
      if let rawString = rawMultiQuoteJSString {
        let startIndex = rawString.startIndex
        let sevenIndex = rawString.index(startIndex, offsetBy: 7)

        var finalString = String(rawString[sevenIndex...])

        finalString = finalString.replacingOccurrences(of: "); return false;", with: "")
        finalString = finalString.replacingOccurrences(of: "'", with: "")

        let quoteComponents = finalString.split(separator: ",")

        let cookieName = "quotes\(quoteComponents[0])-\(quoteComponents[1])-\(quoteComponents[2])"
        print(String(describing: finalString))
        return cookieName
      }

      return nil
    }
    set {
      self.rawMultiQuoteJSString = newValue
    }
  }

  var author: String
  var authorAvatarURL: String?

  var date: String
  var editedAt: String?

  var quoted: String?
  var quotedURL: String?

  var alertURL: String?
  var privateMessageURL: String?
  var starringPostURL: String?
  var userProfileURL: String?

  var iconURL: String?

  var rawNode: String
  var rawContent: String
  var filteredContent: String

  var fromModeration = false
  var deleted = false
  
  init(rawNode: String) {
    self.id = "id"
    self.author = "author"
    self.date = "date"
    self.rawNode = rawNode
    self.rawContent = "rawContent"
    self.filteredContent = "filteredContent"
  }

  func starring() -> Promise<String> {
    return Promise { seal in
      if let starringURL = self.starringPostURL {

        MDN.client.addStarred(withURL: starringURL).done { result in
          return seal.fulfill(result)
        }.catch { error in
          return seal.reject(error)
        }

      } else {
        return seal.reject(MDNException.parsing("Err. No starring URL"))
      }

    }
  }

}

extension Array where Element:Post {
  func toHTML() -> String {
    let postsArray: [String] = self.map { Template.html.post($0) }
    let postsString: String  = postsArray.joined()

    return postsString
  }
}
