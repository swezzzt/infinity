//
//  RehostKit.swift
//  Infinity
//
//  Created by FLK on 11/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//
import Foundation
import UIKit
import Alamofire
import PromiseKit
import Kanna

struct RehostImage {
  var link_full:String
  var link_miniature:String
  var link_preview:String

  var nolink_full:String
  var nolink_miniature:String
  var nolink_preview:String

  var timeStamp = Date()
  var deleted:Bool = false
  var version:Int = 1

  init(links: [String: String]) {
    self.link_full = links["link_full"]!
    self.link_miniature = links["link_miniature"]!
    self.link_preview = links["link_preview"]!

    self.nolink_full = links["nolink_full"]!
    self.nolink_miniature = links["nolink_miniature"]!
    self.nolink_preview = links["nolink_preview"]!
  }
}

enum RehostKitException: Error {
  case error
}

class RehostKit {
  static let client = RehostKit()

  func upload(image: UIImage) -> Promise<RehostImage> {
    print("MDNKit > Upload \(image)")

    return Promise { seal in

      Alamofire.upload(multipartFormData: { (multipartFormData) in
        multipartFormData.append(image.jpegData(compressionQuality: 1)!,
                                 withName: "fichier",
                                 fileName: "snapshot_\(arc4random()).jpg",
                                 mimeType: "image/jpeg")

        multipartFormData.append("Envoyer".data(using: .utf8)!,
                                 withName: "submit")

      }, to: "http://reho.st/upload") {
        (result) in
        switch result {
        case .success(let upload, _, _):

          upload.uploadProgress(closure: { (Progress) in
            print("Upload Progress: \(Progress.fractionCompleted)")
          })

          upload.responseString { response in

            if let html = response.result.value {
              if let doc = try? HTML(html: html, encoding: .utf8) {

                let imageNodes = doc.xpath("//code")
                if imageNodes.count == 8 {

                  var links = [String: String]()
                  links["link_full"] = imageNodes[0].content!
                  links["link_preview"] = imageNodes[2].content!
                  links["link_miniature"] = imageNodes[3].content!

                  links["nolink_full"] = imageNodes[4].content!
                  links["nolink_preview"] = imageNodes[6].content!
                  links["nolink_miniature"] = imageNodes[7].content!

                  seal.fulfill(RehostImage(links: links))

                } else {
                  seal.reject(RehostKitException.error)
                }
              } else {
                seal.reject(RehostKitException.error)
              }
            } else {
              seal.reject(RehostKitException.error)
            }
          }

        case .failure(_):
          seal.reject(RehostKitException.error)

        }
      }

    }
  }
}
