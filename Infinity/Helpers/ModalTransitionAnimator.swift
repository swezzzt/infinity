//
//  ModalTransitionAnimator.swift
//  Infinity
//
//  Created by FLK on 05/10/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
class CustomTopTransition: NSObject, UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 1
  }

  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    print("animateTransition")

    guard let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as? UINavigationController,
      let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as? UINavigationController
      else { return }
    print(fromViewController.topViewController)
    print(toViewController.topViewController)
    let containerView = transitionContext.containerView

    containerView.addSubview(toViewController.view)
    toViewController.view.frame = transitionContext.finalFrame(for: toViewController)
    toViewController.view.frame.origin.y = toViewController.view.frame.height
    UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
      fromViewController.view.alpha = 0.5
      var fromFrame = fromViewController.view.frame
      fromFrame.origin.y += 50
      fromViewController.view.frame = fromFrame
      toViewController.view.frame.origin.y = 0
    }) { finished in
      transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
    }
  }

  func animationEnded(_ transitionCompleted: Bool) {
    print("animationEnded")
    // Clean up our helper object and any additional state
    //transitionDriver = nil
    //initiallyInteractive = false
    //operation = .none
  }

}

class CustomTopTransitionReverse: NSObject, UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 1
  }

  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    print("animateTransition 2")

    guard let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) as? UINavigationController,
      let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) as? UINavigationController
      else { return }

    //let containerView = transitionContext.containerView
    //print(fromViewController.topViewController)
    //print(toViewController.topViewController)
    //containerView.addSubview(toViewController.view)

    //toViewController.view.frame.origin.y = toViewController.view.frame.height
    //fromViewController.view.frame = transitionContext.initialFrame(for: fromViewController)
    //print(transitionContext.initialFrame(for: fromViewController))
    //print(transitionContext.finalFrame(for: fromViewController))
    var finalFrame = transitionContext.finalFrame(for: fromViewController)
    finalFrame.origin.y = finalFrame.height
    UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
      toViewController.view.alpha = 1
      fromViewController.view.frame = finalFrame
    }) { finished in
      transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
    }
  }

  func animationEnded(_ transitionCompleted: Bool) {
    print("animationEnded")
    // Clean up our helper object and any additional state
    //transitionDriver = nil
    //initiallyInteractive = false
    //operation = .none
  }

}

class ModalTransitionAnimator: UIPercentDrivenInteractiveTransition {
  let animator = UIViewPropertyAnimator(duration: 1.0, curve: .linear)

  override init() {
  }
}


extension ModalTransitionAnimator: UIViewControllerAnimatedTransitioning {
  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.5
  }

  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    guard let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
          let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
      else { return }

    UIView.animate(withDuration: transitionDuration(using: transitionContext)) {
      
    }


  }


}

extension ModalTransitionAnimator: UIViewControllerTransitioningDelegate {

}
