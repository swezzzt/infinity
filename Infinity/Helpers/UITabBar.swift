//
//  UITabBar.swift
//  Infinity
//
//  Created by FLK on 29/09/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit
/*
extension UITabBar {
  override open func layoutSubviews() {
    print("UITabBar layoutSubviews override \(traitCollection.horizontalSizeClass)")

    // iOS 11: puts the titles to the right of image for horizontal size class regular. Only want offset when compact.
    // iOS 9 & 10: always puts titles under the image. Always want offset.
    if #available(iOS 11.0, *), traitCollection.horizontalSizeClass == .regular {
      print("UITabBar > horizontalSizeClass REGULAR")
    } else {
      print("UITabBar > horizontalSizeClass Compact")
    }

    super.layoutSubviews()

    // iOS 11: puts the titles to the right of image for horizontal size class regular. Only want offset when compact.
    // iOS 9 & 10: always puts titles under the image. Always want offset.
    var verticalOffset: CGFloat = 6.0

    if #available(iOS 11.0, *), traitCollection.horizontalSizeClass == .regular {
      verticalOffset = 0.0
    }

    let imageInset = UIEdgeInsets(
      top: verticalOffset,
      left: 0.0,
      bottom: -verticalOffset,
      right: 0.0
    )

    for tabBarItem in items ?? [] {
      tabBarItem.title = ""
      tabBarItem.imageInsets = imageInset
    }


  }


}
*/
extension UITabBarItem {



  func showingOnlyImage() -> UITabBarItem {
    // offset to center
    self.imageInsets = UIEdgeInsets(top:6,left:0,bottom:-6,right:0)
    // displace to hide
    self.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 30000)
    return self
  }
}
