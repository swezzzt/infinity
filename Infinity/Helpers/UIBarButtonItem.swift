//
//  UIBarButtonItem.swift
//  Infinity
//
//  Created by FLK on 24/08/2017.
//  Copyright © 2017 flk. All rights reserved.
//

import UIKit

enum UIBarButtonItemLocation {
  case navigationBar
  case toolBar
}

extension UIBarButtonItem {
  convenience init(fontAwesomeName: String, target: Any?, action: Selector?, location: UIBarButtonItemLocation = .toolBar) {
    var size, landscapeSize: CGSize

    switch location {
      case .toolBar:
        size = CGSize(width: 24, height: 24)
        landscapeSize = CGSize(width: 20, height: 20)
      case .navigationBar:
        size = CGSize(width: 26, height: 26)
        landscapeSize = CGSize(width: 22, height: 22)
    }

    self.init(image: UIImage.fontAwesomeIcon(code: fontAwesomeName,
                                             textColor: .black,
                                             size: size),
              landscapeImagePhone: UIImage.fontAwesomeIcon(code: fontAwesomeName,
                                                           textColor: .black,
                                                           size: landscapeSize),
              style: .plain,
              target: target,
              action: action)
  }

  func setImage(fontAwesomeName: String, location: UIBarButtonItemLocation = .toolBar) {
    var size, landscapeSize: CGSize

    switch location {
    case .toolBar:
      size = CGSize(width: 25, height: 25)
      landscapeSize = CGSize(width: 20, height: 20)
    case .navigationBar:
      size = CGSize(width: 30, height: 30)
      landscapeSize = CGSize(width: 22, height: 22)
    }

      self.image = UIImage.fontAwesomeIcon(code: fontAwesomeName,
                                           textColor: .black,
                                           size: size)


      self.landscapeImagePhone = UIImage.fontAwesomeIcon(code: fontAwesomeName,
                                                         textColor: .black,
                                                         size: landscapeSize)

    //self.image.nee
    

  }

  func setTitleTextAttributes(attributes: [NSAttributedString.Key: Any]) {
    self.setTitleTextAttributes(attributes, for: UIControl.State.normal)
    self.setTitleTextAttributes(attributes, for: UIControl.State.highlighted)
    self.setTitleTextAttributes(attributes, for: UIControl.State.disabled)
  }
}
