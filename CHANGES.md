## Next
+ Mise en cache avatar/smileys/icones.
+ Sauvegarde état liste des favoris / filtres.
+ Affichage lien "affiché non filtré" après avoir cliqué sur "cité x fois".
+ Afficher transaction si présent (cat. A/V & co).
+ EgoQuote.

## 0.0.1 #47


## 0.0.1 #46
+ WIP : Cache (Avatars OK)
+ WIP : Envoyer un nouveau MP (Ajout des champs "À"/"Titre", toujours impossible d'envoyer, UI WIP)
+ Le nombre de MP non-lus n'était pas correctement mis à jour.
+ Sujet, Menu contextuel, La fleche pointe maintenant à l'endroit où l'écran a été tapoté.
+ Sujet, Si le sujet est fermé, les boutons répondre/réponse rapide sont remplacés par un cadenas verouillé.

## 0.0.1 #45
+ WIP : Profil utilisateur (Interface KO)
+ WIP : Envoyer un nouveau MP (Interface OK-ish, envoi impossible car pas de titre)
+ Le nombre de MP non-lus est correctement mis à jour lorsqu'il n'y a plus de messages non-lus.
+ Editeur, Les bouttons, messages de confirmation et d'annulation reflètent mieux le contexte (réponse, édition, suppression)
+ Sujet, Le menu contextuel pouvait rester affiché lors du defilement vertical ou horizontal.
+ Sujet, Il fallait parfois toucher l'interface une première fois pour qu'elle réponde correctement (defilement etc.)
+ Sujet, Le style des messages de la moderation à été modifié.
+ Sujet, Menu contextuel, La flèche pointe vers l'avatar.
+ Sujet, Menu contextuel, Ajout des options Supprimer, Copier le lien, Afficher le Profil, Alerter la modération et Envoyer un MP.
+ Sujet, Menu contextuel, Modification des icones pour Profil (gender-neutral :o), Envoyer un MP, Editer & Supprimer.
+ Sujet, Menu contextuel, L'ordre des choix a été modifié pour que les options "Répondre" et "Citer" soient toujours aux mêmes positions :
```
1-2 : "Editer - Suppr." ou "Profil - MP"
3-4 : Toujours "Répondre - Citer"
5   : Toujours "Lien"
6-7 : "Alerte - Favoris" ou <Rien> (MP)
```
###### (L'ajout à la liste noire se fera via le profil)

## 0.0.1 #44
+ Ouverture des liens "untel à écrit" et "cité x fois"
+ Messages de la moderation en rose doré.
+ Dans un MP la bouton "Sondage" en haut à droite a été remplacé par un bouton "Marquer non lu" (non fonctionnel pour le moment).
+ Liste de sujets, lors de l'utilisation du menu contextuel pour aller à une page spécifique, le titre du sujet restait en "gras".
+ Ajout d'une petite fleche vers le bas sur les titres des listes de sujets/catégories.
+ Certaines signatures restaient visibles

## 0.0.1 #43
+ Réponse rapide, réinitialisation après avoir envoyé un message.
+ Liste de sujets, entêtes des pages/catégories plus petites.

## 0.0.1 #42
+ BBCode, L'insertion du code de fin "[/tag]" ne fonctionnait plus.
+ Filtre des titres des topics (ex: [Topic Unique] > [TU])
+ Suppression de la barre d'outils dans les listes catégories/topics, les filtres sont accessibles via la barre de navigation en haut.
+ Boutons factices Sondage/Recherche dans les sujets.
+ Changements cosmétiques ici et là :o

###### Notes #42
Les filtres auront bientôtⓒ un intérêt pour les listes MP/Catégories

## 0.0.1 #41
+ Navigation hybride-hybride 39-40 :o
+ Badge nombre MP non-lus
+ L'actualisation est de nouveau possible via les onglets

###### Notes #41
Le badge n'est pas mis à jour lors de l'actualisation de la liste des MPs.

## 0.0.1 #40
+ Compatible iOS 11+ uniquement / SDK iOS 11.0
+ Navigation hybride v1/v2
+ "Tons" des messages affichés.
+ Corrections d'un bug lors de l'insertion des BBCodes
+ Corrections des incorrehénces au niveaux des couleurs/fonds/polices etc.
+ Menu "Editer" en première position dans le menu contextuel
