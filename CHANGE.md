## Bugs connus
+ Recherche Smiley, L'entête apparait parfois à gauche.
+ Réponse Rapide, Lorsque le clavier apparait, parfois la barre de defilement du sujet est placé en bas de page, parfois pas.
+ Réponse Rapide, La barre de defilement perd les pédales.
+ Tirer pour actualiser dans un topic, Le texte "Tirer pour [...]" reste affiché en bas de page (solution: revenir à la liste et recharger le topic)
+ UI iPad et iPhone en paysage

## 0.0.1 #42
+ BBCode, L'insertion du code de fin "[/tag]" ne fonctionnait plus.
+ Filtre des titres des topics (ex: [Topic Unique] > [TU])
+ Suppression de la barre d'outils dans les listes catégories/topics, les filtres sont accessibles via la barre de navigation en haut.
+ Boutons factices Sondage/Recherche dans les sujets.
+ Changements cosmétiques ici et là :o

###### Notes #42
Les filtres auront bientôtⓒ un intérêt pour les listes MP/Catégories

## 0.0.1 #41
+ Navigation hybride-hybride 39-40 :o
+ Badge nombre MP non-lus
+ L'actualisation est de nouveau possible via les onglets

###### Notes #41
Le badge n'est pas mis à jour lors de l'actualisation de la liste des MPs.

## 0.0.1 #40
+ Compatible iOS 11+ uniquement / SDK iOS 11.0
+ Navigation hybride v1/v2
+ "Tons" des messages affichés.
+ Corrections d'un bug lors de l'insertion des BBCodes
+ Corrections des incorrehénces au niveaux des couleurs/fonds/polices etc.
+ Menu "Editer" en première position dans le menu contextuel

## 0.0.1 #39
Le menu pour insérer les BBCodes déconnait grave donc....
+ Nouveau système d'insertion de BBCode :o

## 0.0.1 #38
+ Le message de la réponse rapide est repris en cas de passage sur la réponse complète.
+ Entêtes des catégories, pages, recherches smileys etc. centrées au lieu d'être alignées à droite.
+ Une légère bordure a été ajoutée en dessous du dernier message lu.
+ Les options Definition/Partager ont été supprimées... dans l'éditeur.
+ Corrigé : Lors de l'actualisation d'un topic, la barre de defilement est positionnée sur le dernier message lu au lieu du bas de page.
+ Corrigé : Editeur, lors de l'annulation la barre de defilement était positionnée en bas de page au lieu de.... de ne rien changer :o
+ Corrigé : Le menu contextuel (Répondre/Citer et BBCode dans l'éditeur) déconnait grave :o

###### Notes #38
Il va y avoir une petite pause dans les builds, le temps de donner un gros coup de polish sur l'interface et l'ergonomie en général.

## 0.0.1 #37
+ Corrigé : L'utilisation du "Tirer pour [..]" ne provoquait plus de vibration (sur les appareils compatibles ~i7 & co).
+ Cats/MP : Pagination via scroll infini.

## 0.0.1 #36
+ Rehost : Nouvelle icone, nouvelle description dans le menu "Partager".
+ Editeur, Ajout d'un effet de flou dans la barre d'outils. (iOS 11)
+ Editeur, Boutons Caméra/Rehost factices :o
+ Dans un Topic, bas de page : Tirer pour actualiser ou Tirer pour charger la page suivante.
+ Corrigé : Saisie d'un numéro de page, Dans certains cas le bouton "Confirmer" restait désactivé.
+ Corrigé : La barre de defilement n'était pas (re)positionnée correctement lors d'une transition entre Liste Topics/Favs/MP.
+ Beta : Détection des majs pour afficher un "Quoi de neuf?" au premier lancement.

## 0.0.1 #35
+ Nouveau "Tirer pour actualiser dans les listes de sujets"
+ Corrigé : Le menu contextuel restait visible lors du retour à la liste des sujets
+ Corrigé : Le menu contextuel pouvait afficher les mauvaises options (BBCode en dehors de l'éditeur, Répondre/Citer sur du texte etc.)
+ Corrigé : Dans l'éditeur la couleur du texte était réglée sur `deep gray` au lieu de `space black` :o

###### Notes #35
Si le menu contextuel affiche toujours les mauvaises options dans certains cas > MP

Le rehost ne gère pas les erreurs lors de l'upload. S'il n'y a pas d'alerte avec un bouton "Copier le lien" > erreur non gérée

## 0.0.1 #34
+ Test

## 0.0.1 #33
+ Extension pour envoyer une image sur HFR Rehost (accessible via menu partager partout dans iOS, avec une belle icone :o)

## 0.0.1 #32
+ Recherche smileys : Gestion de la recherche avec plusieurs mots clés
+ Ajouter un post aux favoris
+ Insertion BBCode
+ Editeur, Message de confirmation lors de l'annulation.

## 0.0.1 #31
- Corrigé : Interfaces daubesques sous iOS 10.3 :fou:

## 0.0.1 #30
+ Citation.
+ Citation Multiple.
+ Editeur, En cas d'erreur, l'option "Ressayer" est fonctionelle.
+ Editeur, En cas d'erreur, l'option "Annuler" a été supprimée.
- Corrigé : Lien vers MP était affiché comme une catégorie.
- Corrigé : Indicateur non lu dans MP.
- Corrigé : Tailles des smileys.
- Corrigé : Smileys, reinitialisation du scroll lors d'une nouvelle recherche.
- Corrigé : Le menu "Editer" ne s'affiche que sur ses propres messages :o

## 0.0.1 #29
+ Editeur, Smileys: Gestion des GIF animés, mise en cache, smileys perso/favoris plus gros

## 0.0.1 #28
+ Editeur, Smileys Beta (Manque gestion des GIF, de la taille des frimousses etc..)
+ Editeur, Amélioration des animations lors de l'annulation

## 0.0.1 #27
+ Réponse Rapide, amélioration des animations.
+ Sujets favoris, les entêtes des catégories sont actives (comme sur la v1).
+ Catégories & sous-catégories, les filtres sont maintenant fonctionnels.

###### Notes #27
Non le wiki smileys n'est pas encore vraiment disponible :o

## 0.0.1 #26
+ Réponse rapide

## 0.0.1 #25
+ Test

## 0.0.1 #24
+ Le bouton "Envoyer" dans l'éditeur est temporairement désactivé si un message est en cours d'ajout/édition
+ Possibilité de supprimer un Favori (via appui long sur un sujet)
+ Transparence désactivée des barres de navigation/d'outils
+ Affichage des sous-catégories
+ "Favoris" renommé en "Sujets"
+ Choix du type de favoris dans "Sujets"
+ Interface pour la réponse rapide

###### Notes #24
Les filtres ne sont pas encore activés pour les (sous-)catégories

## 0.0.1 #23
+ Changements internes au niveau du "Tirer pour actualiser"

## 0.0.1 #22
+ Nouveaux logs pour traquer le bug mentionné en #20
+ Bouton README remplacé par un bouton BUG à utiliser lorsque le cas se présente

## 0.0.1 #21
+ Sujets/Favoris/MP, Titre en gras s'il y a des nouvelles réponses
+ Sujets/Favoris/MP, Affichage des bonnes infos pour chaque sujet (Destinaire, # réponse etc.)

## 0.0.1 #20
+ Ajout d'un remontée de log pour traquer un vilain bug (impossible de charger un sujet depuis la liste Favoris)

## 0.0.1 #19
+ Taille du texte augmentée dans l'éditeur.
+ Le temps d'affichage du message de confirmation lors de l'ajout/edition d'un message a été réduit (de 1s à 50ms).
+ Affichage des categories dans la liste des favoris.
+ Gestion du statut "non lu" dans la liste des MP.
+ Gestion des statuts Épinglé/Fermé dans les listes Sujets/Favoris/MP.
+ Affichage du nombre de citations et de la date d'édition des messages.
- Quand il n'y avait plus de favoris à afficher lors de l'actualisation de la liste, il n'était plus possible de la réactualiser sans fermer l'application.

## 0.0.1 #18
+ Changement de page dans un sujet.
+ Les liens présents dans les messages peuvent être visualisés dans le navigateur interne.
+ La barre de defilement est affichée temporairement après le chargement d'une page.
+ La date du dernier message n'est plus affichée si elle correspond à la date actuelle.
- Les temps de chargement pour l'affichage d'une page et de l'editeur étaient arbitrairement rallongés d'une seconde.
- En attendant leur gestion, désactivation des liens type "Un tel à écrit" dans les citations.

###### Notes #18
Page chargée par défaut: Catégories > Première, Favoris > Drapeau, MP > Dernier message.

Pas encore de gestion des liens, les liens internes au forum se chargent aussi dans le navigateur.

## 0.0.1 #17
+ Afficher / Cacher un spoiler dans un message.
+ Affiche la page en cours / le nombre de total de pages d'un sujet/MP.
+ Liste des sujets d'une cat.
+ Liste des MP.

###### Notes #17
Si après le drapeau en dessous du titre du sujet il y a un numéro, ça charge cette page, sinon ça charge la première.

(En gros MP et Sujets d'une cat > page 1, Favoris > OK)
