## Bugs connus
+ Recherche Smiley, L'entête apparait parfois à gauche.
+ Réponse Rapide, Lorsque le clavier apparait, parfois la barre de defilement du sujet est placé en bas de page, parfois pas.
+ Réponse Rapide, La barre de defilement perd les pédales.
+ Tirer pour actualiser dans un topic, Le texte "Tirer pour [...]" reste affiché en bas de page (solution: revenir à la liste et recharger le topic)
+ Favoris, Element de la liste représentant un topic qui reste en position fixe au lieu de suivre le defilement > Relancer l'application.
+ Sujet, zone "grise" au lieu du contenu du message.
+ UI iPad et iPhone en paysage.
+ Image de lancement (splash screen) tronquée si partage de connexion (Bug iOS)
+ Le badge MP n'est pas mis à jours lors de l'actualisation de la liste des MP > Actualiser liste des favoris ou d'une cat.
+ Les liens s'ouvrent dans Safari au lieu du navigateur interne après avoir suivi un lien type "untel à écrit", "cité x fois" etc.
